package it.pnbtotem.totemcompanionapp.contentunpackservice;

import android.content.*;
import it.pnbtotem.totemcompanionapp.model.Content;
import it.pnbtotem.totemcompanionapp.storage.LocalStorage;
import it.pnbtotem.totemcompanionapp.utilities.StandardService;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * UnpackService:
 * Gets content from the server and unzip them in background.
 */
public class UnpackService extends StandardService {

    public static final String UNPACK_CONTENT_RESULT = "unpackResult";
    public static final String UNPACK_CONTENT_ID_KEY = "contentId";
    private static final String APP_PACKAGE = "it.pnbtotem.totemcompanionapp.";
    public static final String MSG_DO_UNPACK_CONTENT = APP_PACKAGE + "UNPACK_CONTENT";
    public static final String RESPONSE_CONTENT_UNPACKED = APP_PACKAGE + "CONTENT_UNPACKED";

    public static final String MSG_GET_UNPACKED_CONTENT_ID = APP_PACKAGE + "GET_UNPACKED_CONTENT_ID";
    public static final String RESPONSE_UNPACKED_CONTENT_ID = APP_PACKAGE + "UNPACKED_CONTENT_ID";
    private static final String UNZIP_DIRECTORY = "unzipped_content";
    private static final String LAST_UNZIPPED_CONTENT_KEY = "lastUnzippedContent";

    public static File getUnzipDirectory(Context context) {
        return new File(context.getFilesDir(), UNZIP_DIRECTORY);
    }

    public static void unpackContent(Content content, File dir) throws IOException {
        if (!dir.exists()) {
            if (!(dir.mkdirs())) {
                throw new IOException("Can't create directory for content " + content.getId());
            }
        } else if (!dir.isDirectory()) {
            throw new IOException("There is a file with the same name of the target dir for content " + content.getId());
        } else {
            FileUtils.deleteDirectory(dir);
            if (!dir.mkdirs()) {
                throw new IOException("Can't (re)create directory for content " + content.getId());
            }
        }

        ZipFile totemContent = content.getBundle().getAppContentAsZip();
        if (totemContent == null) {
            throw new IOException("Can't load app content");
        }
        Enumeration<? extends ZipEntry> entries = totemContent.entries();
        try {
            while (entries.hasMoreElements()) {
                ZipEntry entry = entries.nextElement();
                if (!entry.isDirectory()) {
                    File f = new File(dir, entry.getName());
                    f.getParentFile().mkdirs();
                    try (InputStream stream = totemContent.getInputStream(entry);
                         OutputStream outputStream = new FileOutputStream(f)) {
                        IOUtils.copy(stream, outputStream);
                    }
                }
            }
        } catch (IOException e) {
            try {
                FileUtils.deleteDirectory(dir);
            } catch (IOException other) {
                other.printStackTrace();
            }

            throw e;
        }
    }

    @Override
    protected void prepareHandlers() {
        setOnMessage(new IntentFilter(MSG_DO_UNPACK_CONTENT), new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Intent result;
                int contentId = intent.getIntExtra(UNPACK_CONTENT_ID_KEY, -1);
                if (contentId == -1) {
                    result = new Intent(RESPONSE_CONTENT_UNPACKED);
                    result.putExtra(UNPACK_CONTENT_RESULT, false);
                    result.putExtra(UNPACK_CONTENT_ID_KEY, contentId);
                } else {
                    try {
                        long lastUnzippedContent;
                        SharedPreferences sharedPref = getSharedPreferences(LAST_UNZIPPED_CONTENT_KEY, Context.MODE_PRIVATE);
                        File targetDirectory = getUnzipDirectory(getApplicationContext());
                        Content content;

                        lastUnzippedContent = sharedPref.getInt(LAST_UNZIPPED_CONTENT_KEY, -1);
                        content = LocalStorage.getInstance().getContent(contentId);
                        if (content == null) {
                            throw new IOException("Can't fetch content (id = " + contentId + ")");
                        }

                        if (lastUnzippedContent != contentId) {
                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.remove(LAST_UNZIPPED_CONTENT_KEY);
                            editor.commit();

                            unpackContent(content, targetDirectory);

                            editor = sharedPref.edit();
                            editor.putInt(LAST_UNZIPPED_CONTENT_KEY, content.getId());
                            editor.apply();
                        }

                        result = new Intent(RESPONSE_CONTENT_UNPACKED);
                        result.putExtra(UNPACK_CONTENT_RESULT, true);
                        result.putExtra(UNPACK_CONTENT_ID_KEY, contentId);
                    } catch (Exception e) {
                        e.printStackTrace();
                        result = new Intent(RESPONSE_CONTENT_UNPACKED);
                        result.putExtra(UNPACK_CONTENT_RESULT, false);
                        result.putExtra(UNPACK_CONTENT_ID_KEY, contentId);
                    }
                }

                sendMessage(result);
            }
        });

        setOnMessage(new IntentFilter(MSG_GET_UNPACKED_CONTENT_ID), new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPref = getSharedPreferences(LAST_UNZIPPED_CONTENT_KEY, Context.MODE_PRIVATE);

                int lastUnzippedContent = sharedPref.getInt(LAST_UNZIPPED_CONTENT_KEY, -1);

                Intent result = new Intent(RESPONSE_UNPACKED_CONTENT_ID);
                result.putExtra(UNPACK_CONTENT_ID_KEY, lastUnzippedContent);

                sendMessage(result);
            }
        });

        setOnClose(new Runnable() {
            @Override
            public void run() {
                System.out.println("Closing unpack service");
            }
        });
    }

    @Override
    protected String getServiceName() {
        return "Content unpacker";
    }
}
