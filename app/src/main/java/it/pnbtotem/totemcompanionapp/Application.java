package it.pnbtotem.totemcompanionapp;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.Context;
import android.content.Intent;
import it.pnbtotem.totemcompanionapp.contentunpackservice.UnpackService;
import it.pnbtotem.totemcompanionapp.geolocation.GeolocationService;
import it.pnbtotem.totemcompanionapp.localcommunication.AdvertisingService;
import it.pnbtotem.totemcompanionapp.localcommunication.SpreaderService;
import it.pnbtotem.totemcompanionapp.serversync.ServerSyncService;

import java.io.File;

/**
 * Application:
 * Application Object
 */
public class Application extends android.app.Application {
    private static Context context;

    public static Context getContext() {
        return context;
    }

    /**
     * Clean all application data
     */
    public static void clearApplicationData() {
        File cacheDirectory = getContext().getCacheDir();
        File applicationDirectory = new File(cacheDirectory.getParent());
        if (applicationDirectory.exists()) {
            String[] fileNames = applicationDirectory.list();
            for (String fileName : fileNames) {
                if (!fileName.equals("lib")) {
                    deleteFile(new File(applicationDirectory, fileName));
                }
            }
        }
    }

    /**
     * Delete a file
     *
     * @param file the file to be deleted
     * @return true if the file was deleted
     */
    private static boolean deleteFile(File file) {
        boolean deletedAll = true;
        if (file != null) {
            if (file.isDirectory()) {
                String[] children = file.list();
                for (int i = 0; i < children.length; i++) {
                    deletedAll = deleteFile(new File(file, children[i])) && deletedAll;
                }
            } else {
                deletedAll = file.delete();
            }
        }

        return deletedAll;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        context = getApplicationContext();
        /*JobScheduler jobScheduler = (JobScheduler) getApplicationContext()
                .getSystemService(Context.JOB_SCHEDULER_SERVICE);
        JobInfo.Builder builder = null;*/

        startService(new Intent(this, UnpackService.class));
        //Esempio di servizio (Standard)
        //1: creare il servizio estendendo la classe StandardService
        //      la classe dovrà avere costruttore vuoto
        //2: implementare i metodi astratti (vedi esempio)
        //4: aggiungere il servizio al manifest (la classe concreta, non quella astratta)
        //5: aggiungere il codice per far partire il servizio (come qui sotto):
        //startService(new Intent(this, StandardServiceExample.class));

        startService(new Intent(this, AdvertisingService.class));

        startService(new Intent(this, SpreaderService.class));

        startService(new Intent(this, GeolocationService.class));

        startService(new Intent(this, ServerSyncService.class));
    }
}
