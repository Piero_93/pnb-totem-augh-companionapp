package it.pnbtotem.totemcompanionapp.totempreview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import it.pnbtotem.totemcompanionapp.R;
import it.pnbtotem.totemcompanionapp.model.ContentMetadata;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by Lorenzo on 25/03/2017.
 */
public class ThumbnailAdapter extends RecyclerView.Adapter<ThumbnailAdapter.MyViewHolder> {

    private PreviewAdapterListener listener;
    private Context mContext;
    private List<ContentMetadata> metadataList;
    private boolean horizontal;

    public ThumbnailAdapter(Context mContext,
                            PreviewAdapterListener listener,
                            boolean horizontal) {
        this.mContext = mContext;
        this.listener = listener;
        this.metadataList = Collections.emptyList();
        this.horizontal = horizontal;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (horizontal) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.preview_card_horizontal, parent, false);
            //((ImageView) itemView.findViewById(R.id.content_thumbnail)).setScaleType(ImageView.ScaleType.FIT_XY);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.preview_card, parent, false);
            //((ImageView) itemView.findViewById(R.id.content_thumbnail)).setScaleType(ImageView.ScaleType.FIT_XY);
        }

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        ContentMetadata metadata = metadataList.get(position);

        byte[] thumbnail = metadata.getThumbnail();
        Bitmap myBitmap = BitmapFactory.decodeByteArray(thumbnail, 0, thumbnail.length);
        holder.thumbnail.setImageBitmap(myBitmap);
    }

    @Override
    public int getItemCount() {
        return metadataList.size();
    }

    public void setMetadataList(Collection<ContentMetadata> newList) {
        this.metadataList = new ArrayList<>(newList);
        notifyDataSetChanged();
    }

    public interface PreviewAdapterListener {
        void onClick(ContentMetadata metadata);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView thumbnail;
        public View view;

        public MyViewHolder(View view) {
            super(view);
            View card = view.findViewById(R.id.preview_card_view);
            this.thumbnail = (ImageView) view.findViewById(R.id.content_thumbnail);
            this.view = view;
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = getAdapterPosition();
                    if (pos < metadataList.size()) {
                        listener.onClick(metadataList.get(pos));
                    }
                }
            });
        }
    }
}
