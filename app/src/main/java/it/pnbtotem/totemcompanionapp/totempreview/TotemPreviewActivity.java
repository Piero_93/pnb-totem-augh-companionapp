package it.pnbtotem.totemcompanionapp.totempreview;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Pair;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import it.pnbtotem.totemcompanionapp.R;
import it.pnbtotem.totemcompanionapp.contentview.ContentSplashActivity;
import it.pnbtotem.totemcompanionapp.contentview.ContentViewActivity;
import it.pnbtotem.totemcompanionapp.model.ContentMetadata;
import it.pnbtotem.totemcompanionapp.serversync.ServerSyncService;
import it.pnbtotem.totemcompanionapp.storage.LocalStorage;
import it.pnbtotem.totemcompanionapp.storage.LocalStorageObserver;
import it.pnbtotem.totemcompanionapp.tagsmanaging.ManageTagsActivity;
import it.pnbtotem.totemcompanionapp.usermanaging.LoginActivity;
import it.pnbtotem.totemcompanionapp.utilities.Utilities;
import org.apache.commons.io.IOUtils;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

import static it.pnbtotem.totemcompanionapp.utilities.SysKB.LOGIN_DONE;

/**
 * TotemPreviewActivity:
 * Show a mirror of the contents shown by the totem
 */
public class TotemPreviewActivity extends AppCompatActivity {

    private static final boolean EXECUTE_TEST = false;
    private FrameLayout previewLayout;
    private RecyclerView previewList;
    private ThumbnailAdapter adapter;
    private LocalStorageObserver storageObserver;

    private ActionBarDrawerToggle mDrawerToggle;

    private BroadcastReceiver syncServiceLoginStatusReceiver;

    private static ContentMetadata createTestMetadata() throws Exception {
        HttpURLConnection connection = null;
        try {
            List<ContentMetadata> list = new ArrayList<>();
            ContentMetadata cm = new ContentMetadata();
            cm.setUrl("ftp://pnb-totem:q1w2e3r4t5y6u7i8o9p0@biaginirouter.myddns.me/C/Documenti/pnb-totem/sampleContent.zip");
            cm.setContentId(12);
            cm.setExpirationDate("2017-05-30T00:00:01.000Z");
            cm.setProviderId("string");
            cm.setTag(new HashSet<String>());
            URL url = new URL("http://www.fnordware.com/superpng/pnggrad16rgba.png");
            connection = (HttpURLConnection) url.openConnection();
            byte[] thumb = IOUtils.toByteArray(connection.getInputStream());
            cm.setThumbnail(thumb);
            cm.setSplashScreen(thumb);
            return cm;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_totem_preview);

        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        final DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        previewLayout = ((FrameLayout) findViewById(R.id.preview_frame));

        //setSupportActionBar(toolbar);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                //getActionBar().setTitle(mTitle);
                //invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //getActionBar().setTitle(mDrawerTitle);
                //invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerLayout.addDrawerListener(mDrawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigation = (NavigationView) findViewById(R.id.lat_menu);
        navigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id) {
                    case R.id.interests_menu:
                        Intent startInterestsActivity = new Intent(TotemPreviewActivity.this,
                                ManageTagsActivity.class);
                        mDrawerLayout.closeDrawers();
                        startActivity(startInterestsActivity);
                        break;
                    case R.id.logout_menu:
                        mDrawerLayout.closeDrawers();
                        doLogout();
                        break;
                }
                return false;
            }
        });

        View headerView = navigation.inflateHeaderView(R.layout.drawer_top);
        TextView usernameText = (TextView) headerView.findViewById(R.id.username_text);
        usernameText.setText(LocalStorage.getInstance().getUserAndPw().first);


        previewList = (RecyclerView) findViewById(R.id.previewElementsList);
        boolean horizontal = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
        orientationReset(horizontal);
    }

    private void doLogout() {
        LocalStorage.getInstance().cleanCredentials();
        LocalStorage.getInstance().cleanContentsAndTags();
        Utilities.clearAll();
        Intent intent = new Intent(TotemPreviewActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        return super.onCreateView(parent, name, context, attrs);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle
        // If it returns true, then it has handled
        // the nav drawer indicator touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onResume() {
        super.onResume();
        storageObserver = new LocalStorageObserver() {
            @Override
            public void totemRefresh(final List<ContentMetadata> metadatas, final int frameNumber) {
                final List<ContentMetadata> metadataList = new ArrayList<>(metadatas);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onTotemChanged(metadataList, frameNumber);
                    }
                });
            }
        };
        LocalStorage.getInstance().addObserver(storageObserver);

        Pair<List<ContentMetadata>, Integer> metadataAndFrameNumber =
                LocalStorage.getInstance().getMetadataAndFrameNum();
        List<ContentMetadata> metadata = metadataAndFrameNumber.first;
        int frameNumber = metadataAndFrameNumber.second;
        onTotemChanged(metadata, frameNumber);

        syncServiceLoginStatusReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                ServerSyncService.LoginResult loginResult = ((ServerSyncService.LoginResult) intent.getSerializableExtra("content"));
                if (loginResult == ServerSyncService.LoginResult.MISSING_CREDENTIALS ||
                        loginResult == ServerSyncService.LoginResult.INVALID_CREDENTIALS) {
                    unregisterBroadcastReceiver();
                    doLogout();
                }
            }
        };

        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(
                syncServiceLoginStatusReceiver,
                new IntentFilter(LOGIN_DONE)
        );

        if (EXECUTE_TEST) {
            loadMockData();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalStorage.getInstance().removeObserver(storageObserver);
        storageObserver = null;

        unregisterBroadcastReceiver();
    }

    private void unregisterBroadcastReceiver() {
        if (syncServiceLoginStatusReceiver != null) {
            LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(
                    syncServiceLoginStatusReceiver
            );

            syncServiceLoginStatusReceiver = null;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        boolean horizontal = newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE;
        orientationReset(horizontal);
    }

    private void orientationReset(boolean horizontal) {
        LinearLayoutManager layoutManager;
        if (horizontal) {
            Resources r = getResources();
            float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, r.getDisplayMetrics());
            previewLayout.setPadding(0, (int) px, 0, 0);
            layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        } else {
            previewLayout.setPadding(0, 0, 0, 0);
            layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        }

        previewList.setLayoutManager(layoutManager);

        adapter = new ThumbnailAdapter(this, new ThumbnailAdapter.PreviewAdapterListener() {
            @Override
            public void onClick(ContentMetadata metadata) {
                //SharedPreferences sharedPref = getSharedPreferences(LAST_UNZIPPED_CONTENT_KEY, Context.MODE_PRIVATE);
                //int lastUnzippedContent = sharedPref.getInt(LAST_UNZIPPED_CONTENT_KEY, -1);

                Intent openContentActivityIntent;
                //if (lastUnzippedContent != metadata.getContentId()) {
                openContentActivityIntent = new Intent(
                        TotemPreviewActivity.this,
                        ContentSplashActivity.class);
                //} else {
                //    openContentActivityIntent = new Intent(
                //            TotemPreviewActivity.this,
                //            ContentViewActivity.class);
                //}

                openContentActivityIntent.putExtra(
                        ContentViewActivity.CONTENT_ID_INTENT_KEY,
                        metadata.getContentId());

                startActivity(openContentActivityIntent);
            }
        }, horizontal);

        previewList.setAdapter(adapter);
        previewList.setHorizontalScrollBarEnabled(horizontal);
        previewList.setVerticalScrollBarEnabled(!horizontal);

        Pair<List<ContentMetadata>, Integer> metadataAndFrameNumber =
                LocalStorage.getInstance().getMetadataAndFrameNum();
        List<ContentMetadata> metadata = metadataAndFrameNumber.first;
        int frameNumber = metadataAndFrameNumber.second;
        onTotemChanged(metadata, frameNumber);

        if (EXECUTE_TEST) {
            loadMockData();
        }
    }

    private void onTotemChanged(Collection<ContentMetadata> contents, int viewSlots) {
        adapter.setMetadataList(contents);
    }

    private void loadMockData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                    ContentMetadata mockMetadata = createTestMetadata();
                    List<ContentMetadata> list = new ArrayList<>();
                    list.add(mockMetadata);
                    LocalStorage.getInstance().setContentMetadata(list, 4);

                    if (storageObserver != null) {
                        storageObserver.totemRefresh(
                                Arrays.asList(
                                        mockMetadata,
                                        mockMetadata,
                                        mockMetadata,
                                        mockMetadata),
                                1);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
