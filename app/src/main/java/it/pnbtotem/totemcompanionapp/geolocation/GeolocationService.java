package it.pnbtotem.totemcompanionapp.geolocation;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import it.pnbtotem.totemcompanionapp.Application;
import it.pnbtotem.totemcompanionapp.utilities.StandardService;

/**
 * GeolocationService:
 * Service that manages the Geolocation in  background
 */
public class GeolocationService extends StandardService {
    public static final int GEOLOCATION_DELAY = 3000;

    public GeolocationService() {
    }

    @Override
    protected void prepareHandlers() {
        Log.v("Geolocation Service", "Scheduling");
        if (PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(Application.getContext(), Manifest.permission.ACCESS_FINE_LOCATION)) {
            GeolocationManager.getInstance().startGeolocation(GEOLOCATION_DELAY);
        } else {
            Log.v("Geolocation Service", "Missing Permissions");
        }
    }

    @Override
    protected String getServiceName() {
        return "Geolocation Service";
    }
}