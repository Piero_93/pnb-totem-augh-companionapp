package it.pnbtotem.totemcompanionapp.geolocation;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import it.pnbtotem.totemcompanionapp.Application;
import it.pnbtotem.totemcompanionapp.internetcommunication.ConnectionException;
import it.pnbtotem.totemcompanionapp.internetcommunication.SwaggerClient;
import it.pnbtotem.totemcompanionapp.storage.LocalStorage;

/**
 * GeolocationManager:
 * Utility to get periodic updates from the Geolocation system (Singleton manager)
 */
public class GeolocationManager {
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0;
    private static final long MIN_TIME_BETWEEN_UPDATES = 0;
    private static GeolocationManager instance;
    private final LocationListener listener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            Log.v("Position", "Lat: " + location.getLatitude() + " Long: " + location.getLongitude());
            try {
                SwaggerClient.setMyPos(LocalStorage.getInstance().getUserAndPw().first, location.getLatitude(), location.getLongitude());
            } catch (ConnectionException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
        }

        @Override
        public void onProviderEnabled(String s) {
        }

        @Override
        public void onProviderDisabled(String s) {
        }
    };
    private LocationManager locationManager;

    /**
     * Get an istance of GeolocationManager (Singleton)
     *
     * @return the single instance
     */
    public static GeolocationManager getInstance() {
        if (instance == null) {
            instance = new GeolocationManager();
        }
        return instance;
    }

    /**
     * Locate the device and call the listener periodically to update the location on the server
     *
     * @param geolocationDelay the (minimum) delay between updates
     */
    public void startGeolocation(int geolocationDelay) {
        if (locationManager == null) {
            locationManager = (LocationManager) Application.getContext().getSystemService(Context.LOCATION_SERVICE);
        }

        boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                geolocationDelay,
                MIN_DISTANCE_CHANGE_FOR_UPDATES,
                listener
        );
        locationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER,
                geolocationDelay,
                MIN_DISTANCE_CHANGE_FOR_UPDATES,
                listener
        );

        if (!isGPSEnabled && !isNetworkEnabled) {
            Log.e("Position", "Location service is disabled!");
        }

    }
}
