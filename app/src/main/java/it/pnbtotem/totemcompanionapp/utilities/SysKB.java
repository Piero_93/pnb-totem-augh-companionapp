package it.pnbtotem.totemcompanionapp.utilities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

/**
 * SysKB:
 * All constants
 */
public class SysKB {
    private SysKB() {}

    //File managing
    public static final String PATH_PREFIX = "contents_spreader/";
    public static final String SPLASH_SCREEN = "splashScreen/splashScreen.png";
    public static final String THUMBNAIL = "thumbnail/thumbnail.png";
    public static final String TOTEM_CONTENT = "totemContent.zip";
    public static final String APP_CONTENT = "appContent.zip";
    public static final String COMPLETE_CONTENT = "content.zip";

    //Server Sync messages
    public static final String LOGIN_MSG = "Login";
    public static final String LOGIN_DONE = "ld";
    public static final String GET_POSSIBLE_INTERESTS_MSG = "gpi";
    public static final String RETURN_POSSIBLE_INTERESTS_MSG = "rpi";
    public static final String SAVE_INTERESTS_MSG = "si";
    public static final String SAVE_INTERESTS_RESULT_MSG = "is";
    public static final String GENERIC_BAD_ERROR_MSG = "gb";
    public static final String SERVER_OFFLINE_MSG = "som";
    public static final String REGISTER_MSG = "rm";
    public static final String REGISTER_RESULT_MSG = "rrm";

    //Content Spreader Service
    public static final String ADVERTISING_DONE_MSG = "advDone";

    public static final String PREFERENCES_NAME = "appPreferences";

    public static final String TOTEM_HOST = "192.168.10.42:8081";

    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    static {
        DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
    }
}
