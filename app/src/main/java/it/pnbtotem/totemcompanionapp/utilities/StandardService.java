package it.pnbtotem.totemcompanionapp.utilities;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import it.pnbtotem.totemcompanionapp.Application;

import java.util.HashMap;
import java.util.Map;

/**
 * StandardService:
 * A generic, abstract service, used to implement other services.
 */
public abstract class StandardService extends Service {

    private volatile Thread serviceThread = null;
    private volatile Looper looper = null;
    private volatile Runnable onClose = null;
    private volatile PeriodicRunnable periodicRunnable = null;
    private volatile Map<BroadcastReceiver, BroadcastReceiver> genericReceivers = null;

    public StandardService() {
    }

    @Override
    public synchronized int onStartCommand(Intent intent, int flags, int startId) {
        if (serviceThread == null) {
            genericReceivers = new HashMap<>();
            serviceThread = new Thread(new Runnable() {
                @Override
                public void run() {

                    Looper.prepare();
                    looper = Looper.myLooper();
                    prepareHandlers();
                    Looper.loop();
                }
            });
            serviceThread.start();
        }
        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public synchronized void onDestroy() {
        if (looper != null) {
            final Runnable onClose = this.onClose;
            final Looper looper = this.looper;
            final Handler handler = new Handler(looper);
            handler.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (onClose != null) {
                            onClose.run();
                        }
                    } finally {
                        looper.quitSafely();
                    }
                }
            });
            this.onClose = null;
            this.looper = null;
            this.serviceThread = null;
            if (periodicRunnable != null) {
                periodicRunnable.stopPeriodic();
            }
            this.periodicRunnable = null;

            for (BroadcastReceiver rec : genericReceivers.values()) {
                LocalBroadcastManager
                        .getInstance(getApplicationContext())
                        .unregisterReceiver(rec);
            }

            genericReceivers = null;
        }

        super.onDestroy();
    }

    protected abstract void prepareHandlers();

    protected abstract String getServiceName();

    protected synchronized void setOnMessage(IntentFilter filter, final BroadcastReceiver receiver) {
        if (genericReceivers == null) {
            return;
        }

        BroadcastReceiver wrapper = new GenericBroadcastReceiver(receiver, looper);

        LocalBroadcastManager
                .getInstance(getApplicationContext())
                .registerReceiver(wrapper, filter);

        genericReceivers.put(receiver, wrapper);
    }

    protected synchronized void removeOnMessage(final BroadcastReceiver receiver) {
        if (genericReceivers == null) {
            return;
        }
        BroadcastReceiver wrapper = genericReceivers.remove(receiver);

        if (wrapper != null) {
            LocalBroadcastManager
                    .getInstance(getApplicationContext())
                    .unregisterReceiver(wrapper);
        }
    }

    protected synchronized void removePeriodic() {
        if (periodicRunnable != null) {
            periodicRunnable.stopPeriodic();
        }
        this.periodicRunnable = null;
    }

    protected void sendMessage(Intent message) {
        LocalBroadcastManager
                .getInstance(getApplicationContext())
                .sendBroadcast(message);
    }

    protected synchronized void setOnClose(final Runnable onClose) {
        this.onClose = onClose;
    }

    protected synchronized void setPeriodic(final int millis, final Runnable toBeRun) {
        setPeriodic(millis, toBeRun, true);
    }

    protected synchronized void setPeriodic(final int millis,
                                            final Runnable toBeRun,
                                            boolean scheduleImmediately) {
        final Handler handler = new Handler(/*looper*/);
        periodicRunnable = new PeriodicRunnable(toBeRun, looper, millis, scheduleImmediately);
        handler.postDelayed(periodicRunnable, millis);
    }

    protected void sendDebugString(String message) {
        Intent data = new Intent();
        data.setAction(getClass().getName());
        data.putExtra("output", message);
        LocalBroadcastManager.getInstance(Application.getContext()).sendBroadcast(data);
    }

    private class PeriodicRunnable implements Runnable {

        private Runnable toBeRun;
        private Looper looper;
        private long millis;
        private boolean immediately;

        public PeriodicRunnable(Runnable toBeRun,
                                Looper looper,
                                long millis,
                                boolean immediately) {
            this.toBeRun = toBeRun;
            this.looper = looper;
            this.millis = millis;
            this.immediately = immediately;
        }

        @Override
        public void run() {
            try {
                if (immediately) {
                    toBeRun.run();
                } else {
                    immediately = true;
                }
            } finally {
                if (millis >= 0) {
                    Handler handler = new Handler(looper);
                    handler.postDelayed(this, millis);
                }
            }
        }

        public void stopPeriodic() {
            millis = -1;
        }
    }

    private class GenericBroadcastReceiver extends BroadcastReceiver {

        BroadcastReceiver receiver;
        Looper myLooper;

        public GenericBroadcastReceiver(BroadcastReceiver receiver, Looper looper) {
            this.receiver = receiver;
            this.myLooper = looper;
        }

        @Override
        public void onReceive(final Context context, final Intent intent) {
            final Handler handler = new Handler(myLooper);
            handler.post(new Runnable() {
                @Override
                public void run() {
                    receiver.onReceive(context, intent);
                }
            });
        }

    }
}
