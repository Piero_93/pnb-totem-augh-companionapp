package it.pnbtotem.totemcompanionapp.utilities;

import android.content.Context;
import it.pnbtotem.totemcompanionapp.Application;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.Calendar;

import static it.pnbtotem.totemcompanionapp.utilities.SysKB.DATE_FORMAT;
import static it.pnbtotem.totemcompanionapp.utilities.SysKB.PREFERENCES_NAME;

/**
 * Utilities:
 * All utilities.
 */
public class Utilities {
    /**
     * Copies an input stream on an output stream
     *
     * @param input  the source
     * @param output the destination
     * @throws IOException
     */
    public static void copyStream(InputStream input, OutputStream output) throws IOException {
        int nRead;
        byte[] data = new byte[1024];
        while ((nRead = input.read(data, 0, data.length)) != -1) {
            output.write(data, 0, nRead);
        }
    }

    /**
     * Clear all shared preferences
     */
    public static void clearAll() {
        Application.getContext().getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE).edit().clear().apply();
        Application.clearApplicationData();
    }

    public static Calendar stringToDate(String string) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(DATE_FORMAT.parse(string));
        return calendar;
    }

    public static String dateToString(Calendar calendar) {
        return DATE_FORMAT.format(calendar.getTime());
    }
}
