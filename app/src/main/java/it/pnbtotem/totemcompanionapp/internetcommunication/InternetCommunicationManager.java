package it.pnbtotem.totemcompanionapp.internetcommunication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import io.swagger.client.model.ContentMetaData;
import it.pnbtotem.totemcompanionapp.model.Content;
import it.pnbtotem.totemcompanionapp.model.ContentBundleImpl;
import it.pnbtotem.totemcompanionapp.serversync.ServerSyncService;
import it.pnbtotem.totemcompanionapp.utilities.SysKB;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;

/**
 * InternetCommunicationManager:
 * Manages every communication between the phone and internet (i.e. getting content from the provider's servers).
 */
public class InternetCommunicationManager {
    private static final boolean INTERNET = true;//For test purposes
    private static InternetCommunicationManager instance;
    private boolean serverOnline = true;
    private BroadcastReceiver receiver;

    private InternetCommunicationManager() {
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {
                    case SysKB.LOGIN_DONE:
                        ServerSyncService.LoginResult result = (ServerSyncService.LoginResult) intent.getSerializableExtra("content");
                        System.out.println(result);
                        switch (result) {
                            case LOGIN_OFFLINE:
                                serverOnline = false;
                                break;
                            default:
                                serverOnline = true;
                                break;

                        }
                        break;
                    case SysKB.GENERIC_BAD_ERROR_MSG:
                        break;
                }
            }
        };
    }

    public static InternetCommunicationManager getInstance() {
        if (instance == null) {
            instance = new InternetCommunicationManager();
        }
        return instance;
    }

    /**
     * Fetch a zipFile from the specified IP Address
     *
     * @param address the file address
     * @return the read zipFile
     * @throws IOException
     */
    private static byte[] fetchContent(String address) throws IOException {
        if(!INTERNET) {
            throw new SocketTimeoutException("Internet offline test");
        }

        URL url = new URL(address);
        URLConnection con = url.openConnection();

        try (InputStream inputStream = con.getInputStream()) {
             return IOUtils.toByteArray(inputStream);
        }
    }

    /**
     * Try to download the bundle file for a content. If already present, do nothing
     *
     * @param content the content with and empty bundle
     * @return the content with the bundle
     * @throws Exception
     */
    public Content getFullContent(Content content) throws Exception {
        if (content.getBundle() != null) {
            return content;
        } else {
            return new Content(content.getId(),
                    content.getProviderId(),
                    content.getTags(),
                    content.getUrl(),
                    content.getExpirationDate(),
                    content.getRevNumber(),
                    new ContentBundleImpl(content.getId(), fetchContent(content.getUrl())));
        }
    }

    public int getLastRevForContent(int id) throws ConnectionException {
        ContentMetaData metadata = SwaggerClient.getContentWithId(String.valueOf(id));
        return metadata.getRev();
    }

    public boolean isServerOnline() {
        return INTERNET && serverOnline;
    }
}
