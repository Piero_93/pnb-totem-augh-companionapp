package it.pnbtotem.totemcompanionapp.internetcommunication;

/**
 * ConnectionException:
 * Thrown when something bad happens while contacting the Web Service.
 */
public class ConnectionException extends Exception {
    private int errorCode;
    private String message;

    /**
     * Constructor
     *
     * @param errorCode the HTTP Error Code
     * @param message   the related message
     */
    public ConnectionException(int errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    /**
     * Get the HTTP Error Code
     *
     * @return the HTTP Error Code
     */
    public int getErrorCode() {
        return errorCode;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
