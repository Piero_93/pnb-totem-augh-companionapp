package it.pnbtotem.totemcompanionapp.internetcommunication;

import io.swagger.client.ApiException;
import io.swagger.client.api.DefaultApi;
import io.swagger.client.model.ContentMetaData;
import io.swagger.client.model.Credentials;
import io.swagger.client.model.Position;
import io.swagger.client.model.RegistrationErrors;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Biagini on 13/03/2017.
 */
public class SwaggerClient {
    private static DefaultApi instance;

    private SwaggerClient() {
    }

    private static void checkInstance() {
        if (instance == null) {
            instance = new DefaultApi();
        }
    }

    /**
     * Login
     *
     * @param username the username
     * @param password the password
     * @return the response HTTP Code
     */
    public static int loginUser(String username, String password) {
        checkInstance();

        instance.getApiClient().setUsername(username);
        instance.getApiClient().setPassword(password);

        //The user is not logged only if the status code is 401.
        //If code is 200 the user is logged (and the user exists).
        //If code is 404 the user is logged (but the user does not exists).
        try {
            System.out.println(instance.userUserIdInterestsGET(username));
            return 200;
        } catch (ApiException e) {
            return e.getCode();
        }
    }

    /**
     * Register a new user
     *
     * @param name     username (id)
     * @param password password
     * @return null if the registration was successful, a RegistrationError if not
     * @throws ConnectionException
     */
    public static RegistrationErrors registerNewUser(String name, String password) throws ConnectionException {
        checkInstance();

        Credentials cred = new Credentials();
        cred.setUserId(name);
        cred.setPassword(password);

        try {
            instance.userPOST(cred);
            return null;
        } catch (ApiException e) {
            if (e.getCode() == 422) {
                RegistrationErrors registrationError = new RegistrationErrors();
                try {
                    JSONObject o = new JSONObject(e.getResponseBody());
                    registrationError.setDuplicatedUsername(o.optBoolean("duplicatedUsername"));
                    registrationError.setTooShortPassword(o.optBoolean("tooShortPassword"));
                    registrationError.setTooShortUsername(o.optBoolean("tooShortUsername"));
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
                return registrationError;
            } else {
                throw new ConnectionException(e.getCode(), e.getMessage());
            }
        }
    }

    /**
     * Fetch all tags
     *
     * @return the set of tags present on the server
     * @throws ConnectionException
     */
    public static Set<String> fetchAllTags() throws ConnectionException {
        checkInstance();

        try {
            return new HashSet<>(instance.tagsGET());
        } catch (ApiException e) {
            throw new ConnectionException(e.getCode(), e.getMessage());
        }
    }

    /**
     * Fetch all tags for a specified user
     *
     * @param myUserId the user id
     * @return the set of tags selected by the user
     * @throws ConnectionException
     */
    public static Set<String> fetchMyTags(String myUserId) throws ConnectionException {
        checkInstance();

        try {
            return new HashSet<>(instance.userUserIdInterestsGET(myUserId));
        } catch (ApiException e) {
            throw new ConnectionException(e.getCode(), e.getMessage());
        }
    }

    /**
     * Set tags for a specified user
     *
     * @param myUserId the user id
     * @param tags     the new set of tags
     * @throws ConnectionException
     */
    public static void setMyTags(String myUserId, Set<String> tags) throws ConnectionException {
        checkInstance();

        try {
            instance.userUserIdInterestsPOST(myUserId, new ArrayList<>(tags));
        } catch (ApiException e) {
            throw new ConnectionException(e.getCode(), e.getMessage());
        }
    }

    /**
     * Set position for a specified user
     *
     * @param myUserId  the user id
     * @param latitude  the latitude
     * @param longitude the longitude
     * @throws ConnectionException
     */
    public static void setMyPos(String myUserId, double latitude, double longitude) throws ConnectionException {
        checkInstance();

        Position position = new Position();
        position.setLatitude(latitude);
        position.setLongitude(longitude);

        try {
            instance.userUserIdPositionPOST(myUserId, position);
        } catch (ApiException e) {
            throw new ConnectionException(e.getCode(), e.getMessage());
        }
    }

    /**
     * Get all existing interests
     *
     * @return a set containing all interests
     * @throws ConnectionException
     */
    public static Set<String> getAllTags() throws ConnectionException {
        checkInstance();

        try {
            return new HashSet<>(instance.tagsGET());
        } catch (ApiException e) {
            throw new ConnectionException(e.getCode(), e.getMessage());
        }
    }

    /**
     * Get the metadata for a content with a specified Id
     *
     * @param id the content id
     * @return the metadata for the content with the specified Id
     * @throws ConnectionException something bad happened while requesting
     */
    public static ContentMetaData getContentWithId(String id) throws ConnectionException {
        checkInstance();

        try {
            return instance.contentContentIdGET(id);
        } catch (ApiException e) {
            throw new ConnectionException(e.getCode(), e.getMessage());
        }
    }
}
