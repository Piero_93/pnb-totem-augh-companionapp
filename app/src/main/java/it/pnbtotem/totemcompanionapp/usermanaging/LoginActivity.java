package it.pnbtotem.totemcompanionapp.usermanaging;

import android.Manifest;
import android.app.AlertDialog;
import android.content.*;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import it.pnbtotem.totemcompanionapp.R;
import it.pnbtotem.totemcompanionapp.serversync.ServerSyncService;
import it.pnbtotem.totemcompanionapp.storage.LocalStorage;
import it.pnbtotem.totemcompanionapp.totempreview.TotemPreviewActivity;
import it.pnbtotem.totemcompanionapp.utilities.KeyboardUtils;
import it.pnbtotem.totemcompanionapp.utilities.SysKB;

/**
 * LoginActivity:
 * Log in Activity
 */
public class LoginActivity extends AppCompatActivity {
    public static final String USERNAME = "log_username";
    public static final String PASSWORD = "log_password";
    public static final String CALLER = "caller";
    private static final int PERMISSION = 42;
    private TextView usernameTextView;
    private TextView passwordTextView;
    private BroadcastReceiver receiver;

    private Class activityCaller = null;

    @Nullable
    private AlertDialog currentlyDisplayedDialog = null;

    @Override
    @SuppressWarnings("Duplicates")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {
                    case SysKB.LOGIN_DONE:
                        findViewById(R.id.progressBar).setVisibility(View.GONE);
                        ServerSyncService.LoginResult result = (ServerSyncService.LoginResult) intent.getSerializableExtra("content");
                        System.out.println(result);
                        switch (result) {
                            case LOGIN_OFFLINE:
                                LocalStorage.getInstance().cleanCredentials();
                                showOfflineLoginAlert();
                                break;
                            case LOGIN_OK:
                                startMainActivity();
                                break;
                            case INVALID_CREDENTIALS:
                                LocalStorage.getInstance().cleanCredentials();
                                showLoginError();
                            case MISSING_CREDENTIALS:
                                LocalStorage.getInstance().cleanCredentials();
                        }
                        break;
                    case SysKB.GENERIC_BAD_ERROR_MSG:
                        finish();
                        break;
                }
                LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(receiver);
            }
        };

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            showNewDialog(new AlertDialog.Builder(this)
                    .setMessage(getString(R.string.permission_message))
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION);
                        }
                    }));
        }

        usernameTextView = (TextView) findViewById(R.id.txt_username_login);
        passwordTextView = (TextView) findViewById(R.id.txt_password_login);

        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            activityCaller = (Class) intent.getExtras().get(CALLER);

            SpannableString username = (SpannableString) intent.getExtras().get(USERNAME);
            SpannableString password = (SpannableString) intent.getExtras().get(PASSWORD);

            if (username != null && username.length() != 0) {
                usernameTextView.setText(username);
            }

            if (password != null && password.length() != 0) {
                passwordTextView.setText(username);
            }
        }

        //Try autologin, if succeeded start the app
        sendBroadcastMsg(SysKB.LOGIN_MSG);

        Button loginButton = (Button) findViewById(R.id.btn_login);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
                LocalStorage.getInstance().setUserAndPw(usernameTextView.getText().toString().trim(), passwordTextView.getText().toString());
                sendBroadcastMsg(SysKB.LOGIN_MSG);
            }
        });

        final TextView registerLink = (TextView) findViewById(R.id.txt_register_link_login);
        registerLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent registrationViewIntent = new Intent(LoginActivity.this, RegistrationActivity.class);
                if (usernameTextView.getText().length() != 0) {
                    registrationViewIntent.putExtra(RegistrationActivity.USERNAME, usernameTextView.getText());
                }
                if (passwordTextView.getText().length() != 0) {
                    registrationViewIntent.putExtra(RegistrationActivity.PASSWORD, passwordTextView.getText());
                }
                startActivity(registrationViewIntent);
            }
        });

        KeyboardUtils.addKeyboardToggleListener(this, new KeyboardUtils.SoftKeyboardToggleListener() {
            @Override
            public void onToggleSoftKeyboard(boolean isVisible) {
                Log.d("keyboard", "keyboard visible: " + isVisible);
                findViewById(R.id.img_logo).setVisibility(isVisible ? View.GONE : View.VISIBLE);
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (!(requestCode == PERMISSION && grantResults.length == 2 &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
            finish();

        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        dismissPreviousDialog();
    }

    private void sendBroadcastMsg(String msg) {
        registerToListener();
        Intent data = new Intent();
        data.setAction(msg);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(data);
    }

    private void registerToListener() {
        setOnMessage(new IntentFilter(SysKB.LOGIN_DONE), receiver); //Contenuto: credenziali non valide con 401, loginUser ok con 404 o 200, loginUser offline con 503
        setOnMessage(new IntentFilter(SysKB.GENERIC_BAD_ERROR_MSG), receiver); //Errore nel richiedere qualcosa che non è il loginUser: sono loggato ma per qualche motivo strano lo username è sbagliato
    }

    private void setOnMessage(IntentFilter filter, BroadcastReceiver receiver) {
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(receiver, filter);
    }

    private void dismissPreviousDialog() {
        if (currentlyDisplayedDialog != null) {
            currentlyDisplayedDialog.dismiss();
            currentlyDisplayedDialog = null;
        }
    }

    private void showNewDialog(AlertDialog.Builder builder) {
        dismissPreviousDialog();

        currentlyDisplayedDialog = builder.create();
        currentlyDisplayedDialog.show();
    }

    private void showOfflineLoginAlert() {
        showNewDialog(new AlertDialog.Builder(this)
                .setMessage(getString(R.string.offline_login_message))
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                }));
    }

    private void showLoginError() {
        showNewDialog(new AlertDialog.Builder(this)
                .setMessage(getString(R.string.not_valid_credentials_message))
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                }));
    }

    private void startMainActivity() {
        Intent mainOrCallerViewIntent = new Intent(LoginActivity.this, activityCaller == null ? TotemPreviewActivity.class : activityCaller);
        mainOrCallerViewIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(mainOrCallerViewIntent);
        finish();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        System.out.println("Changing");
        System.out.println(newConfig.keyboard);
        System.out.println(newConfig.keyboardHidden);
    }


}