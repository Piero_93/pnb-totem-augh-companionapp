package it.pnbtotem.totemcompanionapp.usermanaging;

import android.app.AlertDialog;
import android.content.*;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import io.swagger.client.model.RegistrationErrors;
import it.pnbtotem.totemcompanionapp.R;
import it.pnbtotem.totemcompanionapp.storage.LocalStorage;
import it.pnbtotem.totemcompanionapp.utilities.KeyboardUtils;
import it.pnbtotem.totemcompanionapp.utilities.SysKB;

/**
 * RegistrationActivity:
 * Registration Activity
 */
public class RegistrationActivity extends AppCompatActivity {
    public static final String USERNAME = "reg_username";
    public static final String PASSWORD = "reg_password";

    private TextView usernameTextView;
    private TextView passwordTextView;

    private BroadcastReceiver receiver;

    @Override
    @SuppressWarnings("Duplicates")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {
                    case SysKB.REGISTER_RESULT_MSG:
                        RegistrationErrors result = (RegistrationErrors) intent.getSerializableExtra("content");
                        System.out.println(result);
                        if (result == null) { //Registration OK
                            onSuccessfulRegistration();
                        } else {
                            onRegistrationError(result);
                        }
                        break;
                    case SysKB.GENERIC_BAD_ERROR_MSG:
                        finish();
                        break;
                }
                LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(receiver);
            }
        };

        usernameTextView = (TextView) findViewById(R.id.txt_username_registration);
        passwordTextView = (TextView) findViewById(R.id.txt_password_registration);

        Button registerButton = (Button) findViewById(R.id.btn_register);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
                LocalStorage.getInstance().setUserAndPw(usernameTextView.getText().toString().trim(), passwordTextView.getText().toString());
                sendBroadcastMsg(SysKB.REGISTER_MSG);
            }
        });

        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            SpannableString username = (SpannableString) intent.getExtras().get(USERNAME);
            SpannableString password = (SpannableString) intent.getExtras().get(PASSWORD);

            if (username != null && username.length() != 0) {
                usernameTextView.setText(username);
            }

            if (password != null && password.length() != 0) {
                passwordTextView.setText(username);
            }
        }

        KeyboardUtils.addKeyboardToggleListener(this, new KeyboardUtils.SoftKeyboardToggleListener() {
            @Override
            public void onToggleSoftKeyboard(boolean isVisible) {
                Log.d("keyboard", "keyboard visible: " + isVisible);
                findViewById(R.id.img_logo).setVisibility(isVisible ? View.GONE : View.VISIBLE);
            }
        });
    }

    private void sendBroadcastMsg(String msg) {
        registerToListener();
        Intent data = new Intent();
        data.setAction(msg);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(data);
    }

    private void registerToListener() {
        setOnMessage(new IntentFilter(SysKB.REGISTER_RESULT_MSG), receiver);
        setOnMessage(new IntentFilter(SysKB.GENERIC_BAD_ERROR_MSG), receiver);
    }

    private void setOnMessage(IntentFilter filter, BroadcastReceiver receiver) {
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(receiver, filter);
    }

    private void onSuccessfulRegistration() {
        Intent loginViewIntent = new Intent(RegistrationActivity.this, LoginActivity.class);
        loginViewIntent.putExtra(LoginActivity.USERNAME, usernameTextView.getText());
        loginViewIntent.putExtra(LoginActivity.PASSWORD, passwordTextView.getText());
        startActivity(loginViewIntent);
    }

    private void onRegistrationError(RegistrationErrors result) {
        findViewById(R.id.progressBar).setVisibility(View.GONE);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getString(R.string.registration_error));
        if (result.getDuplicatedUsername()) {
            stringBuilder.append("\n");
            stringBuilder.append(getString(R.string.registration_error_duplicated_username));
        }
        if (result.getTooShortUsername()) {
            stringBuilder.append("\n");
            stringBuilder.append(getString(R.string.registration_error_short_username));
        }
        if (result.getTooShortPassword()) {
            stringBuilder.append("\n");
            stringBuilder.append(getString(R.string.registration_error_short_password));
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("" +
                stringBuilder.toString())
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                }).create().show();
    }
}
