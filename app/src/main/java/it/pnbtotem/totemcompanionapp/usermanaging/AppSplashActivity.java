package it.pnbtotem.totemcompanionapp.usermanaging;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.*;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import it.pnbtotem.totemcompanionapp.R;
import it.pnbtotem.totemcompanionapp.serversync.ServerSyncService;
import it.pnbtotem.totemcompanionapp.totempreview.TotemPreviewActivity;
import it.pnbtotem.totemcompanionapp.utilities.SysKB;


/**
 * AppSplashActivity:
 * Splash Screen
 */
public class AppSplashActivity extends Activity {
    private BroadcastReceiver receiver;

    private AlertDialog dialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_splash);
    }

    @Override
    protected void onStart() {
        super.onStart();

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {
                    case SysKB.LOGIN_DONE:
                        ServerSyncService.LoginResult result = (ServerSyncService.LoginResult) intent.getSerializableExtra("content");
                        System.out.println(result);
                        switch (result) {
                            case LOGIN_OFFLINE:
                                showOfflineLoginAlert();
                                break;
                            case LOGIN_OK:
                                startMainActivity();
                                break;
                            case INVALID_CREDENTIALS:
                            case MISSING_CREDENTIALS:
                                showLoginError();
                                break;
                        }
                        break;
                    case SysKB.GENERIC_BAD_ERROR_MSG:
                        finish();
                        break;
                }

                unregisterBroadcastReceiver();
            }
        };

        sendBroadcastMsg(SysKB.LOGIN_MSG);
    }

    @Override
    protected void onStop() {
        super.onStop();


        unregisterBroadcastReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }

    private void unregisterBroadcastReceiver() {
        if (receiver != null) {
            LocalBroadcastManager.getInstance(getApplicationContext())
                    .unregisterReceiver(receiver);

            receiver = null;
        }
    }

    private void sendBroadcastMsg(String msg) {
        registerToListener();
        Intent data = new Intent();
        data.setAction(msg);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(data);
    }

    private void registerToListener() {
        setOnMessage(new IntentFilter(SysKB.LOGIN_DONE), receiver); //Contenuto: credenziali non valide con 401, loginUser ok con 404 o 200, loginUser offline con 503
        setOnMessage(new IntentFilter(SysKB.GENERIC_BAD_ERROR_MSG), receiver); //Errore nel richiedere qualcosa che non è il loginUser: sono loggato ma per qualche motivo strano lo username è sbagliato
    }

    private void setOnMessage(IntentFilter filter, BroadcastReceiver receiver) {
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(receiver, filter);
    }

    private void showOfflineLoginAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        dialog = builder.setMessage("" +
                "Offline loginUser: you are currently logged offline.")
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        startMainActivity();
                    }
                })
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                }).create();

        dialog.show();
    }

    private void showLoginError() {
        Intent mainOrCallerViewIntent = new Intent(this, LoginActivity.class);
        mainOrCallerViewIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(mainOrCallerViewIntent);
    }

    private void startMainActivity() {
        Intent mainOrCallerViewIntent = new Intent(this, TotemPreviewActivity.class);
        mainOrCallerViewIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(mainOrCallerViewIntent);
        finish();
    }
}
