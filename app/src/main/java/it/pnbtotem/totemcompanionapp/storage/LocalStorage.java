package it.pnbtotem.totemcompanionapp.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.util.Pair;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import it.pnbtotem.totemcompanionapp.Application;
import it.pnbtotem.totemcompanionapp.internetcommunication.ConnectionException;
import it.pnbtotem.totemcompanionapp.internetcommunication.InternetCommunicationManager;
import it.pnbtotem.totemcompanionapp.model.Content;
import it.pnbtotem.totemcompanionapp.model.ContentBundleImpl;
import it.pnbtotem.totemcompanionapp.model.ContentMetadata;
import it.pnbtotem.totemcompanionapp.model.ContentMetadataWithLocalPath;
import it.pnbtotem.totemcompanionapp.utilities.SysKB;

import java.io.*;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.*;

import static it.pnbtotem.totemcompanionapp.utilities.Utilities.dateToString;
import static it.pnbtotem.totemcompanionapp.utilities.Utilities.stringToDate;

/**
 * LocalStorage:
 * Implements the internal data storage of the companion app, by managing json files with tags and contents
 */
public class LocalStorage {

    private static final String USERNAME_TEXT_PREFS = "username";
    private static final String PASSWORD_TEXT_PREFS = "password";
    private static final String TAGS_FILE_NAME = "interests.conf";
    private static final String CONTENTS_FILE_NAME = "contents.conf";
    private static final String DIRECTORY_CONTENTS_NAME = "contents";
    private static final String METADATA_FILE_NAME = "metadata.cappconf";


    private static LocalStorage localStorage;

    private volatile String username;
    private volatile String password;

    private volatile Set<String> tags;
    private HashMap<Integer, Content> currentContents;
    private List<ContentMetadata> currentMetadata;
    private int frameNumber;
    private Set<Integer> allContentsInMemory;
    private List<LocalStorageObserver> observers;

    /**
     * Constructor:
     * initialize the shared preferences
     */
    private LocalStorage() {
        this.observers = new ArrayList<>();
        SharedPreferences prefs = Application.getContext().getSharedPreferences(SysKB.PREFERENCES_NAME, Context.MODE_PRIVATE);

        this.username = prefs.getString(USERNAME_TEXT_PREFS, null);
        this.password = prefs.getString(PASSWORD_TEXT_PREFS, null);

        Gson json = new Gson();
        Type collectionType = new TypeToken<Set<String>>() {
        }.getType();
        /*
        Type collectionType2 = new TypeToken<HashMap<Integer, Pair<Content, Date>>>() {
        }.getType();
        */
        try {
            String s = this.readFromFile(Application.getContext().getFilesDir().toString() + "/" + TAGS_FILE_NAME);
            if (!s.equals("")) {
                this.tags = json.fromJson(s, collectionType);
            } else {
                this.tags = new HashSet<>();
            }

        } catch (JsonSyntaxException e) {
            this.tags = new HashSet<>();
        }

        /*
        json = new Gson();
        try {
            String s = this.readFromFile(CONTENTS_FILE_NAME);
            if (!s.equals("")) {
                this.currentContents = json.fromJson(this.readFromFile(CONTENTS_FILE_NAME), collectionType2);
            } else {
                this.currentContents = new HashMap<>();
            }
        } catch (JsonSyntaxException e) {
            this.currentContents = new HashMap<>();
        }*/
        this.currentContents = new HashMap<>();
        this.currentMetadata = new ArrayList<>();

        this.allContentsInMemory = new HashSet<>();
        String path = Application.getContext().getFilesDir().toString() + "/" + DIRECTORY_CONTENTS_NAME;
        File directory = new File(path);
        File[] files = directory.listFiles();
        if (files != null) {
            for (File file : files) {
                int id = Integer.parseInt(file.getName().replace(".json", ""));
                allContentsInMemory.add(id);
            }
        }
    }

    /**
     * Get the singleton instance
     *
     * @return the instante
     */
    public static synchronized LocalStorage getInstance() {
        if (localStorage == null) {
            localStorage = new LocalStorage();
        }

        return localStorage;
    }

    /**
     * Get user and password
     *
     * @return username and password is they are present, null otherwise
     */
    public synchronized Pair<String, String> getUserAndPw() {
        return new Pair<>(username, password);
    }

    /**
     * Set the credentials
     *
     * @param username the username
     * @param password the password
     */
    public synchronized void setUserAndPw(String username, String password) {
        Application.getContext().getSharedPreferences(SysKB.PREFERENCES_NAME, Context.MODE_PRIVATE).edit()
                .putString(USERNAME_TEXT_PREFS, username)
                .putString(PASSWORD_TEXT_PREFS, password)
                .apply();
        this.username = username;
        this.password = password;
    }

    /**
     * Delete current credentials
     */
    public synchronized void cleanCredentials() {
        Application.getContext().getSharedPreferences(SysKB.PREFERENCES_NAME, Context.MODE_PRIVATE).edit()
                .remove(USERNAME_TEXT_PREFS)
                .remove(PASSWORD_TEXT_PREFS)
                .apply();
        this.username = null;
        this.password = null;
    }

    /**
     * Delete current contents and tags
     */
    public synchronized void cleanContentsAndTags() {
        this.currentContents.clear();
        this.currentMetadata.clear();
        this.tags.clear();
    }

    /**
     * Get user's selected tags
     *
     * @return user's interests
     */
    public synchronized Set<String> getTags() {
        return this.tags;
    }

    /**
     * Set user's selected tags
     *
     * @param tags a set of selected tags
     */
    public synchronized void setTags(Set<String> tags) {
        Gson json = new Gson();
        this.writeToFile(TAGS_FILE_NAME, json.toJson(tags));
        this.tags = tags;
    }

    /**
     * Get a stored content
     *
     * @param id the content id
     * @return the content corresponding the @id
     */
    public Content getContent(int id) {
        if (currentContents.containsKey(id)) {
            try {
                boolean internet = InternetCommunicationManager.getInstance().isServerOnline();
                try {
                    internet = internet && InternetCommunicationManager.getInstance().getLastRevForContent(id) == currentContents.get(id).getRevNumber();
                } catch (ConnectionException e) {
                    internet = true;
                }

                if (currentContents.get(id).getExpirationDate().after(Calendar.getInstance())
                        && currentContents.get(id).getBundle() != null && internet) {
                    return currentContents.get(id);
                } else {
                    if (InternetCommunicationManager.getInstance().isServerOnline()) {
                        Content fullContent = InternetCommunicationManager.getInstance().getFullContent(currentContents.get(id));
                        ContentMetadataWithLocalPath out = new ContentMetadataWithLocalPath();
                        out.setContentId(fullContent.getId());
                        out.setProviderId(fullContent.getProviderId());
                        out.setTag(fullContent.getTags());
                        out.setUrl(fullContent.getUrl());
                        out.setRev(fullContent.getRevNumber());
                        out.setLocalPath(fullContent.getBundle().getLocalPath());
                        out.setExpirationDate(dateToString(fullContent.getExpirationDate()));
                        Gson json = new Gson();
                        this.writeToFile(DIRECTORY_CONTENTS_NAME + "/" + generateFileNameForContent(id), json.toJson(
                                out
                        ));
                        this.allContentsInMemory.add(id);
                        return fullContent;
                    } else {
                        return null;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (allContentsInMemory.contains(id)) {
            Gson json = new Gson();
            String jsonContent = readFromFile(Application.getContext().getFilesDir().toString() + "/" + DIRECTORY_CONTENTS_NAME + "/" + generateFileNameForContent(id));
            ContentMetadataWithLocalPath metadataWithLocalPath = json.fromJson(jsonContent, ContentMetadataWithLocalPath.class);

            try {
                Content content = new Content(
                        metadataWithLocalPath.getContentId(),
                        metadataWithLocalPath.getProviderId(),
                        metadataWithLocalPath.getTag(),
                        metadataWithLocalPath.getUrl(),
                        stringToDate(metadataWithLocalPath.getExpirationDate()),
                        metadataWithLocalPath.getRev(),
                        new ContentBundleImpl(metadataWithLocalPath.getLocalPath())
                );
                this.currentContents.put(content.getId(), content);
                return content;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }

    /**
     * Set current metadata with input data
     *
     * @param metadata    a list of content metadata
     * @param frameNumber current frame number
     */
    public void setContentMetadata(List<ContentMetadata> metadata, int frameNumber) {
        this.currentMetadata = metadata;
        this.frameNumber = frameNumber;
        for (ContentMetadata contentMetadata : metadata) {
            try {
                if((!currentContents.containsKey(contentMetadata.getContentId()) ||
                        currentContents.get(contentMetadata.getContentId()).getRevNumber() < contentMetadata.getRev()) &&
                        !allContentsInMemory.contains(contentMetadata.getContentId())) {
                    this.currentContents.put(contentMetadata.getContentId(), new Content(
                            contentMetadata.getContentId(),
                            contentMetadata.getProviderId(),
                            contentMetadata.getTag(),
                            contentMetadata.getUrl(),
                            stringToDate(contentMetadata.getExpirationDate()),
                            contentMetadata.getRev()
                    ));
                }
            } catch (ParseException e) {
                throw new IllegalArgumentException();
            }
        }

        for (LocalStorageObserver observer : observers) {
            observer.totemRefresh(metadata, frameNumber);
        }
    }

    public List<ContentMetadata> getAllExistingMetadata() {
        List<ContentMetadata> list = new ArrayList<>();
        Gson json = new Gson();

        for (Integer id : allContentsInMemory) {
            String jsonContent = readFromFile(Application.getContext().getFilesDir().toString() + "/" + DIRECTORY_CONTENTS_NAME + "/" + generateFileNameForContent(id));
            list.add(json.fromJson(jsonContent, ContentMetadataWithLocalPath.class));
        }

        return list;
    }

    /**
     * Saves permanently in memory a content
     *
     * @param content
     */
    public void saveContent(Content content) {
        ContentMetadataWithLocalPath out = new ContentMetadataWithLocalPath();
        out.setContentId(content.getId());
        out.setProviderId(content.getProviderId());
        out.setTag(content.getTags());
        out.setUrl(content.getUrl());
        out.setRev(content.getRevNumber());
        out.setLocalPath(content.getBundle().getLocalPath());
        out.setExpirationDate(dateToString(content.getExpirationDate()));
        //currentContents.put(fullContent.getId(), fullContent);
        Gson json = new Gson();
        this.writeToFile(DIRECTORY_CONTENTS_NAME + "/" + generateFileNameForContent(content.getId()), json.toJson(
                out
        ));
        this.currentContents.put(content.getId(), content);
        this.allContentsInMemory.add(content.getId());
    }

    /**
     * Add a new observer to the storage
     *
     * @param observer the new observer
     */
    public void addObserver(LocalStorageObserver observer) {
        this.observers.add(observer);
    }

    /**
     * Removes an observer from the list of observer
     *
     * @param observer the observer to  be removed
     */
    public void removeObserver(LocalStorageObserver observer) {
        this.observers.remove(observer);
    }

    /**
     * Write a string into a file
     *
     * @param fileName the file name
     * @param text     the string to be written
     */
    private void writeToFile(String fileName, String text) {
        FileWriter fileWriter;
        try {
            File file = new File(Application.getContext().getFilesDir(), fileName);
            file.getParentFile().mkdirs();
            fileWriter = new FileWriter(new File(Application.getContext().getFilesDir(), fileName));
            fileWriter.write(text);
            fileWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @return the list of current metadatas and the frame number of the totem
     */
    public Pair<List<ContentMetadata>, Integer> getMetadataAndFrameNum() {
        return new Pair<>(this.currentMetadata, this.frameNumber);
    }

    private String readFromFile(String fileName) {
        try (InputStream inputStream = new FileInputStream(fileName)) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String receiveString;
            StringBuilder stringBuilder = new StringBuilder();

            while ((receiveString = bufferedReader.readLine()) != null) {
                stringBuilder.append(receiveString);
            }

            return stringBuilder.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private static String generateFileNameForContent(int id) {
        return id + ".json";
    }
}
