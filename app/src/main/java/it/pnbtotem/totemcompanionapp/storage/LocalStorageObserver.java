package it.pnbtotem.totemcompanionapp.storage;

import it.pnbtotem.totemcompanionapp.model.ContentMetadata;

import java.util.List;

/**
 * LocalStorageObserver:
 * Observer for the Local Storage
 */
public interface LocalStorageObserver {
    void totemRefresh(List<ContentMetadata> metadatas, int frameNumber);
}
