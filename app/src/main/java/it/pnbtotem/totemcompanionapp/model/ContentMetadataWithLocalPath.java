package it.pnbtotem.totemcompanionapp.model;

/**
 * ContentMetadataWithLocalPath:
 * Content metadata with the path to the local file, serializable to JSON.
 */
public class ContentMetadataWithLocalPath extends ContentMetadata {
    private String localPath;

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }
}
