package it.pnbtotem.totemcompanionapp.model;

import java.util.List;

/**
 * ResponseToUserAdvertise:
 * Serializable class that wrap the totem response to an advertise
 */
public class ResponseToUserAdvertise {
    private List<ContentMetadataWithoutThumbnailAndSplash> contents;
    private int maxContentsOnScreen;

    public ResponseToUserAdvertise() {
    }

    /**
     * Get the visualized contents metadata
     *
     * @return a list of metadata
     */
    public List<ContentMetadataWithoutThumbnailAndSplash> getContents() {
        return contents;
    }

    /**
     * Set the visualized contents
     *
     * @param contents the contents metadata
     */
    public void setContents(List<ContentMetadataWithoutThumbnailAndSplash> contents) {
        this.contents = contents;
    }

    /**
     * Get the maximum number of content on the screen
     *
     * @return the max number of content on the screen
     */
    public int getMaxContentsOnScreen() {
        return maxContentsOnScreen;
    }

    /**
     * Set the maximum number of content on the screen
     *
     * @param maxContentsOnScreen the maximum number of content on the screen
     */
    public void setMaxContentsOnScreen(int maxContentsOnScreen) {
        this.maxContentsOnScreen = maxContentsOnScreen;
    }
}
