package it.pnbtotem.totemcompanionapp.model;

import java.util.List;

/**
 * LocalUserIncomingSpreadBundle:
 * Contains the list of content that the user possess
 */
public class LocalUserIncomingSpreadBundle {
    private String userId;
    private List<ContentMetadataWithoutThumbnailAndSplash> contents;

    public LocalUserIncomingSpreadBundle() {}

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<ContentMetadataWithoutThumbnailAndSplash> getContents() {
        return contents;
    }

    public void setContents(List<ContentMetadataWithoutThumbnailAndSplash> contents) {
        this.contents = contents;
    }
}
