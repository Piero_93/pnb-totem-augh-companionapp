package it.pnbtotem.totemcompanionapp.model;

import java.util.List;

/**
 * ResponseToUserSpread:
 * Contains the list of contents to be sent to the totem and to be received from it
 */
public class ResponseToUserSpread {
    private List<Integer> toBeSentToTotem;
    private List<Integer> toBeSentToApp;

    public ResponseToUserSpread() {}

    public List<Integer> getToBeSentToTotem() {
        return toBeSentToTotem;
    }

    public void setToBeSentToTotem(List<Integer> toBeSentToTotem) {
        this.toBeSentToTotem = toBeSentToTotem;
    }

    public List<Integer> getToBeSentToApp() {
        return toBeSentToApp;
    }

    public void setToBeSentToApp(List<Integer> toBeSentToApp) {
        this.toBeSentToApp = toBeSentToApp;
    }
}
