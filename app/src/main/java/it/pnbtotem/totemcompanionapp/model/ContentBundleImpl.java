package it.pnbtotem.totemcompanionapp.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import it.pnbtotem.totemcompanionapp.Application;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static it.pnbtotem.totemcompanionapp.utilities.SysKB.*;

/**
 * ContentBundleImpl:
 * An implementation for ContentBundle.
 */
public class ContentBundleImpl implements ContentBundle {
    private File localPath;

    public ContentBundleImpl(int id, @NonNull byte[] zipBytes) throws Exception {
        this.localPath = new File(Application.getContext().getFilesDir() + "/" + PATH_PREFIX + id + "/");

        FileOutputStream completeContentOutputStream = null;
        FileOutputStream splashScreenOutputStream = null;
        FileOutputStream thumbnailOutputStream = null;
        FileOutputStream totemContentOutputStream = null;
        FileOutputStream appContentOutputStream = null;
        ZipFile zipFile = null;

        try {
            //Complete zip
            File completeContentFile = new File(localPath, COMPLETE_CONTENT);
            {
                completeContentFile.getParentFile().mkdirs();
                completeContentOutputStream = new FileOutputStream(completeContentFile);
                completeContentOutputStream.write(zipBytes);
                completeContentOutputStream.close();
                zipFile = new ZipFile(completeContentFile);
            }

            //Splash Screen
            {
                InputStream splashScreenInputStream = zipFile.getInputStream(zipFile.getEntry(SPLASH_SCREEN));
                File splashScreenFile = new File(localPath, SPLASH_SCREEN);
                splashScreenFile.getParentFile().mkdirs();
                splashScreenOutputStream = new FileOutputStream(splashScreenFile);
                IOUtils.copy(splashScreenInputStream, splashScreenOutputStream);
                splashScreenInputStream.close();
                splashScreenOutputStream.close();
            }

            //Thumbnail
            {
                InputStream thumbnailInputStream = zipFile.getInputStream(zipFile.getEntry(THUMBNAIL));
                File thumbnailFile = new File(localPath, THUMBNAIL);
                thumbnailFile.getParentFile().mkdirs();
                thumbnailOutputStream = new FileOutputStream(new File(localPath, THUMBNAIL));
                IOUtils.copy(thumbnailInputStream, thumbnailOutputStream);
                thumbnailInputStream.close();
                thumbnailOutputStream.close();
            }

            //TotemContent && AppContent
            File totemContentFile = new File(localPath, TOTEM_CONTENT);
            totemContentFile.getParentFile().mkdirs();
            File appContentFile = new File(localPath, APP_CONTENT);
            appContentFile.getParentFile().mkdirs();
            totemContentOutputStream = new FileOutputStream(totemContentFile);
            appContentOutputStream = new FileOutputStream(appContentFile);

            Enumeration<? extends ZipEntry> entries = zipFile.entries();

            while (entries.hasMoreElements()) {
                ZipEntry e = entries.nextElement();
                if (e.getName().startsWith("totemContent")) {
                    IOUtils.copy(zipFile.getInputStream(e), totemContentOutputStream);
                } else if (e.getName().startsWith("appContent")) {
                    IOUtils.copy(zipFile.getInputStream(e), appContentOutputStream);
                }
            }
        } catch (NullPointerException | IOException e) {
            try {
                FileUtils.deleteDirectory(localPath);
            } catch (IOException | IllegalArgumentException e1) {
                e1.printStackTrace();
            }
            throw new IllegalArgumentException("Zip File content is not a valid Content for PNB-Totem", e);
        } finally {
            closeStream(zipFile);
            closeStream(splashScreenOutputStream);
            closeStream(totemContentOutputStream);
            closeStream(thumbnailOutputStream);
            closeStream(appContentOutputStream);
            closeStream(completeContentOutputStream);
        }

        try {
            isContentValid();
        } catch (Exception e) {
            FileUtils.deleteDirectory(localPath);
            throw e;
        }
    }

    public ContentBundleImpl(String localPath) throws Exception {
        this.localPath = new File(localPath);

        isContentValid();
    }

    @Override
    public Bitmap getSplashScreen() {
        return BitmapFactory.decodeFile(new File(localPath, SPLASH_SCREEN).getAbsolutePath());
    }

    @Override
    public Bitmap getThumbnail() {
        return BitmapFactory.decodeFile(new File(localPath, THUMBNAIL).getAbsolutePath());
    }

    @Override
    public byte[] getTotemContent() {
        File f = new File(localPath, TOTEM_CONTENT);
        try {
            return FileUtils.readFileToByteArray(f);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public byte[] getAppContent() {
        File f = new File(localPath, APP_CONTENT);
        try {
            return FileUtils.readFileToByteArray(f);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ZipFile getTotemContentAsZip() {
        try {
            return new ZipFile(new File(localPath, TOTEM_CONTENT));
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public ZipFile getAppContentAsZip() {
        try {
            return new ZipFile(new File(localPath, APP_CONTENT));
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public ZipFile getContentZip() {
        try {
            return new ZipFile(new File(localPath, COMPLETE_CONTENT));
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public byte[] getContentBytes() {
        File f = new File(localPath, COMPLETE_CONTENT);
        try {
            return FileUtils.readFileToByteArray(f);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String getLocalPath() {
        return localPath.getAbsolutePath();
    }

    @Override
    public String toString() {
        return "ContentBundleImpl{" +
                "localPath='" + localPath + '\'' +
                '}';
    }

    private void closeStream(Closeable c) {
        if( c != null ) {
            try {
                c.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void isContentValid() throws Exception {
        boolean splashScreenExists = this.getSplashScreen() != null;
        boolean thumbnailExists = this.getThumbnail() != null;
        boolean totemContentExists = this.getTotemContent() != null;
        boolean appContentExists = this.getAppContent() != null;
        boolean completeContentExists = this.getContentBytes() != null;

        //If the content is not valid, throws exception
        if(!(splashScreenExists &&
                thumbnailExists &&
                totemContentExists &&
                appContentExists &&
                completeContentExists)) {
            System.out.println("Local path exists = " + localPath.exists());
            System.out.println("Local path isDirectory = " + localPath.isDirectory());
            if(localPath.isDirectory()) {
                for (File file : localPath.listFiles()) {
                    System.out.println("File: " + file);
                }
            }

            System.out.println("splash = " + splashScreenExists);
            System.out.println("thumbnailExists = " + thumbnailExists);
            System.out.println("totemContentExists = " + totemContentExists);
            System.out.println("appContentExists = " + appContentExists);
            System.out.println("completeContentExists = " + completeContentExists);
            throw new IllegalArgumentException("Content path is not valid: " + localPath);
        }
    }
}
