package it.pnbtotem.totemcompanionapp.model;

import android.support.annotation.NonNull;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

/**
 * Content:
 * Models a Content and its metadata.
 */
public class Content {
    private int id;
    private String providerId;
    private Set<String> tags;
    private String url;
    private Calendar expirationDate;
    private int revNumber;
    private ContentBundle bundle;

    /**
     * Constructor
     *
     * @param id             content id
     * @param providerId     provider id
     * @param tags           set of tags
     * @param url            location of the content
     * @param expirationDate expiration date
     * @param revNumber      revision number
     */
    public Content(int id,
                   @NonNull String providerId,
                   @NonNull Set<String> tags,
                   @NonNull String url,
                   @NonNull Calendar expirationDate,
                   int revNumber) {
        if (providerId == null || tags == null || url == null || expirationDate == null) {
            throw new IllegalArgumentException("ProviderId, Tags, Url or ExpirationDate are null");
        }
        this.id = id;
        this.providerId = providerId;
        this.tags = tags;
        this.url = url;
        this.expirationDate = expirationDate;
        this.revNumber = revNumber;
    }

    /**
     * Constructor
     *
     * @param id             content id
     * @param providerId     provider id
     * @param tags           set of tags
     * @param url            location of the content
     * @param expirationDate expiration date
     * @param revNumber      revision number
     * @param bundle         content data
     */
    public Content(int id, @NonNull String providerId, @NonNull Set<String> tags, @NonNull String url, @NonNull Calendar expirationDate, @NonNull int revNumber, @NonNull ContentBundle bundle) {
        this(id, providerId, tags, url, expirationDate, revNumber);
        if (bundle == null) {
            throw new IllegalArgumentException("Bundle is null");
        }
        this.bundle = bundle;
    }

    /**
     * Get the id
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Get the provider id
     *
     * @return the provider id
     */
    public String getProviderId() {
        return providerId;
    }

    /**
     * Get all tags assigned to this content
     *
     * @return a set of tag
     */
    public Set<String> getTags() {
        return new HashSet<>(tags);
    }

    /**
     * Get the location (url) of the content
     *
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Get the expiration date
     *
     * @return the expiration date
     */
    public Calendar getExpirationDate() {
        return (Calendar) expirationDate.clone();
    }

    /**
     * Get the content data
     *
     * @return the bundle
     */
    public ContentBundle getBundle() {
        return bundle;
    }

    /**
     * Get the revision number
     *
     * @return the revision number
     */
    public int getRevNumber() {
        return revNumber;
    }

    @Override
    public String toString() {
        return "Content{" +
                "id=" + id +
                ", providerId='" + providerId + '\'' +
                ", tags=" + tags +
                ", url='" + url + '\'' +
                ", expirationDate=" + expirationDate +
                ", revNumber=" + revNumber +
                ", bundle=" + bundle +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Content content = (Content) o;

        if (id != content.id) return false;
        if (providerId != null ? !providerId.equals(content.providerId) : content.providerId != null) return false;
        if (tags != null ? !tags.equals(content.tags) : content.tags != null) return false;
        if (url != null ? !url.equals(content.url) : content.url != null) return false;
        if (expirationDate != null ? !expirationDate.equals(content.expirationDate) : content.expirationDate != null)
            return false;
        return bundle != null ? bundle.equals(content.bundle) : content.bundle == null;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
