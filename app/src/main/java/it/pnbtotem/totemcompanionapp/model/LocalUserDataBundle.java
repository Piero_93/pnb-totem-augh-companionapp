package it.pnbtotem.totemcompanionapp.model;

import java.util.Set;

/**
 * LocalUserDataBundle:
 * All user's data
 */
public class LocalUserDataBundle {
    private String userId;
    private Set<String> tag;

    public LocalUserDataBundle() {

    }

    /**
     * Get the user id
     *
     * @return the user id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Set the user id
     *
     * @param userId the user id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * Get all tags selected by the user
     *
     * @return a set of tags
     */
    public Set<String> getTag() {
        return tag;
    }

    /**
     * Set new tags for a user
     *
     * @param tag the new set of tags
     */
    public void setTag(Set<String> tag) {
        this.tag = tag;
    }
}
