package it.pnbtotem.totemcompanionapp.model;

/**
 * LocalUserIncomingPullRequest:
 * Request for a content sent from app to totem
 */
public class LocalUserIncomingPullRequest {
    private String userId;
    private int contentId;

    public LocalUserIncomingPullRequest() {}

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getContentId() {
        return contentId;
    }

    public void setContentId(int contentId) {
        this.contentId = contentId;
    }
}
