package it.pnbtotem.totemcompanionapp.model;

import java.util.Set;

/**
 * ContentMetadata:
 * Content metadata, serializable to JSON.
 */
public class ContentMetadataWithoutThumbnailAndSplash {
    private int contentId;
    private String providerId;
    private String url;
    private String expirationDate;
    private Set<String> tag;
    private int rev;

    public ContentMetadataWithoutThumbnailAndSplash() {

    }

    public int getContentId() {
        return contentId;
    }

    public void setContentId(int contentId) {
        this.contentId = contentId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Set<String> getTag() {
        return tag;
    }

    public void setTag(Set<String> tag) {
        this.tag = tag;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public int getRev() {
        return rev;
    }

    public void setRev(int rev) {
        this.rev = rev;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ContentMetadataWithoutThumbnailAndSplash that = (ContentMetadataWithoutThumbnailAndSplash) o;

        if (contentId != that.contentId) return false;
        if (rev != that.rev) return false;
        if (!providerId.equals(that.providerId)) return false;
        if (!url.equals(that.url)) return false;
        if (!expirationDate.equals(that.expirationDate)) return false;
        return tag.equals(that.tag);
    }

    @Override
    public int hashCode() {
        int result = contentId;
        result = 31 * result + providerId.hashCode();
        result = 31 * result + url.hashCode();
        result = 31 * result + expirationDate.hashCode();
        result = 31 * result + tag.hashCode();
        result = 31 * result + rev;
        return result;
    }
}
