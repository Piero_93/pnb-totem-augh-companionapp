package it.pnbtotem.totemcompanionapp.tagsmanaging;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.*;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;
import it.pnbtotem.totemcompanionapp.R;
import it.pnbtotem.totemcompanionapp.serversync.ServerSyncService;
import it.pnbtotem.totemcompanionapp.storage.LocalStorage;
import it.pnbtotem.totemcompanionapp.usermanaging.LoginActivity;
import it.pnbtotem.totemcompanionapp.utilities.SysKB;
import it.pnbtotem.totemcompanionapp.utilities.Utilities;

import java.util.*;

/**
 * ManageTagsActivity:
 * Activity to manage your interests (add, remove, search)
 */
public class ManageTagsActivity extends AppCompatActivity {
    public static final int OFFLINE_RETRY_TIME = 1500;
    private BroadcastReceiver receiver;

    private TagsListWithCheckAdapter adapter;
    private List<Pair<String, Boolean>> currentTags;
    private List<Pair<String, Boolean>> allTags;

    private String query = "";

    private AlertDialog dialog = null;

    private AsyncTask<Void, Void, Void> offlineTagFetchRetry = null;

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        sendBroadcastMsg(SysKB.GET_POSSIBLE_INTERESTS_MSG);

        //Get the intent and extract the search query
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            query = intent.getStringExtra(SearchManager.QUERY);
        }

        if (query == null) {
            query = "";
        }

        updateView();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_tags);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        currentTags = new ArrayList<>();
        allTags = new ArrayList<>();

        ListView tagsListView = (ListView) findViewById(R.id.search_list);
        adapter = new TagsListWithCheckAdapter(this, currentTags,
                new TagsListWithCheckAdapter.TagSelectionListener() {
                    @Override
                    public void onTagSelection(String tag, boolean isSelected) {
                        Set<String> myTags = new HashSet<>();
                        Pair<String, Boolean> toBeChanged = null;
                        for (Pair<String, Boolean> tagWithCheck : allTags) {
                            if (tagWithCheck.first.equals(tag)) {
                                toBeChanged = tagWithCheck;
                                break;
                            }
                        }

                        if (toBeChanged == null) {
                            return;
                        }

                        allTags.remove(toBeChanged);
                        allTags.add(new Pair<>(tag, isSelected));

                        Collections.sort(allTags, new Comparator<Pair<String, Boolean>>() {
                            @Override
                            public int compare(Pair<String, Boolean> a, Pair<String, Boolean> b) {
                                return a.first.compareTo(b.first);
                            }
                        });

                        for (Pair<String, Boolean> tagWithCheck : allTags) {
                            if (tagWithCheck.second) {
                                myTags.add(tagWithCheck.first);
                            }
                        }

                        sendBroadcastMsg(SysKB.SAVE_INTERESTS_MSG, myTags);

                        updateView();
                    }
                });

        tagsListView.setAdapter(adapter);

        updateView();
    }

    @Override
    public void onPause() {
        super.onPause();

        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        sendBroadcastMsg(SysKB.GET_POSSIBLE_INTERESTS_MSG);
    }

    @Override
    public void onStop() {
        super.onStop();
        unscheduleRetry();
        unregisterBroadcastReceiver();
    }

    private void scheduleRetry() {
        offlineTagFetchRetry = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... objects) {
                try {
                    Thread.sleep(OFFLINE_RETRY_TIME);
                } catch (InterruptedException ignored) {
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                sendBroadcastMsg(SysKB.GET_POSSIBLE_INTERESTS_MSG);
            }
        };

        offlineTagFetchRetry.execute(null, null);
    }

    private void unscheduleRetry() {
        if (offlineTagFetchRetry != null) {
            offlineTagFetchRetry.cancel(false);
            offlineTagFetchRetry = null;
        }
    }

    private void unregisterBroadcastReceiver() {
        if (receiver != null) {
            LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(receiver);
            receiver = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search_menu, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.search));
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                query = newText;
                updateView();

                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                //Log.i("onQueryTextSubmit", query);

                return true;
            }
        };
        searchView.setOnQueryTextListener(queryTextListener);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != R.id.search) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void showReceivedTags(Set<String> tags) {
        Set<Pair<String, Boolean>> queriedTask = new HashSet<>();

        for (String tag : tags) {
            //if(query == null  || tag.contains(query)) {
            queriedTask.add(new Pair<>(tag, LocalStorage.getInstance().getTags().contains(tag)));
            //}
        }

        allTags.clear();
        allTags.addAll(queriedTask);

        Collections.sort(allTags, new Comparator<Pair<String, Boolean>>() {
            @Override
            public int compare(Pair<String, Boolean> a, Pair<String, Boolean> b) {
                return a.first.compareTo(b.first);
            }
        });

        updateView();
    }

    private void sendBroadcastMsg(String msg) {
        registerToListener();
        Intent data = new Intent();
        data.setAction(msg);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(data);
    }

    private void sendBroadcastMsg(String msg, Collection<String> tags) {
        registerToListener();
        Intent data = new Intent();
        data.setAction(msg);
        data.putExtra("content", tags.toArray(new String[tags.size()]));
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(data);
    }

    private void registerToListener() {
        if (receiver != null) {
            return;
        }

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {
                    case SysKB.LOGIN_DONE:
                        ServerSyncService.LoginResult loginResult = (ServerSyncService.LoginResult) intent.getSerializableExtra("content");
                        if (ServerSyncService.LoginResult.INVALID_CREDENTIALS.equals(loginResult) ||
                                ServerSyncService.LoginResult.MISSING_CREDENTIALS.equals(loginResult)) {
                            unregisterBroadcastReceiver();
                            onLoginError();
                        }
                        break;
                    case SysKB.RETURN_POSSIBLE_INTERESTS_MSG:
                        Set<String> tags = new HashSet<>();
                        Object[] tagsResult = (Object[]) intent.getSerializableExtra("content");
                        for (Object item : tagsResult) {
                            tags.add((String) item);
                        }
                        showReceivedTags(tags);

                        unregisterBroadcastReceiver();
                        break;
                    case SysKB.SERVER_OFFLINE_MSG:
                        showServerOfflineError();
                        scheduleRetry();
                        break;
                    case SysKB.GENERIC_BAD_ERROR_MSG:
                        unregisterBroadcastReceiver();
                        finish();
                        break;
                }
            }
        };


        setOnMessage(new IntentFilter(SysKB.LOGIN_DONE), receiver);
        setOnMessage(new IntentFilter(SysKB.RETURN_POSSIBLE_INTERESTS_MSG), receiver);
        setOnMessage(new IntentFilter(SysKB.GENERIC_BAD_ERROR_MSG), receiver);
        setOnMessage(new IntentFilter(SysKB.SERVER_OFFLINE_MSG), receiver);
    }

    private void setOnMessage(IntentFilter filter, BroadcastReceiver receiver) {
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(receiver, filter);
    }

    private void showServerOfflineError() {
        Toast.makeText(this, R.string.offline_error, Toast.LENGTH_SHORT).show();
    }

    private void onLoginError() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        dialog = builder.setMessage("Your credentials are invalid. Please, log in.")
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        LocalStorage.getInstance().cleanCredentials();
                        LocalStorage.getInstance().cleanContentsAndTags();
                        Utilities.clearAll();
                        Intent intent = new Intent(ManageTagsActivity.this, LoginActivity.class);
                        intent.putExtra(LoginActivity.CALLER, this.getClass());
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                })
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                }).create();

        dialog.show();
    }

    private void updateView() {
        currentTags.clear();
        for (Pair<String, Boolean> allTag : allTags) {
            if (allTag.first.contains(query)) {
                currentTags.add(new Pair<>(allTag.first, allTag.second));
            }
        }

        adapter.notifyDataSetChanged();
        if (currentTags == null || currentTags.size() == 0) { //If no tags are present, show the textView
            findViewById(R.id.no_interests_text_search).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.no_interests_text_search).setVisibility(View.GONE);
        }
    }

    /*@Override
    public void onBackPressed() {
        if(query.isEmpty()) {
            //query = null;
            Intent intent = new Intent(this, ManageTagsActivity.class);
            startActivity(intent);
        } else {
            super.onBackPressed();
        }
    }*/
}
