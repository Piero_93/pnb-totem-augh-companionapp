package it.pnbtotem.totemcompanionapp.tagsmanaging;

import android.content.Context;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import it.pnbtotem.totemcompanionapp.R;

import java.util.List;

/**
 * Created by Biagini on 22/03/2017.
 */
public class TagsListWithCheckAdapter extends ArrayAdapter<Pair<String, Boolean>> {
    private TagSelectionListener listener;

    public TagsListWithCheckAdapter(Context context,
                                    List<Pair<String, Boolean>> objects,
                                    TagSelectionListener listener) {
        super(context, R.layout.tag_row, R.id.tag_item, objects);
        this.listener = listener;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        final ViewHolderWithCheckBox holder;

        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.tag_row_with_checkbox, null);
            holder = new ViewHolderWithCheckBox();
            holder.tagName = (TextView) v.findViewById(R.id.tag_item);
            holder.originalTagName = (TextView) v.findViewById(R.id.original_tag_item);
            holder.checkBox = (CheckBox) v.findViewById(R.id.checkBox);

            holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    listener.onTagSelection(holder.originalTagName.getText().toString(), b);
                }
            });

            v.setTag(holder);
        } else {
            holder = (ViewHolderWithCheckBox) v.getTag();
        }

        holder.tagName.setText("#" + this.getItem(position).first.toLowerCase());
        holder.originalTagName.setText(this.getItem(position).first);
        holder.checkBox.setChecked(this.getItem(position).second);
        holder.checkBox.jumpDrawablesToCurrentState();

        return v;
    }

    public interface TagSelectionListener {
        void onTagSelection(String tag, boolean isSelected);
    }
}

class ViewHolderWithCheckBox {
    TextView tagName;
    TextView originalTagName;
    CheckBox checkBox;
}


