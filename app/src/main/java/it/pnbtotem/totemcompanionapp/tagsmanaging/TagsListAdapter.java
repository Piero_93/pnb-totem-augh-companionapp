package it.pnbtotem.totemcompanionapp.tagsmanaging;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import it.pnbtotem.totemcompanionapp.R;

import java.util.List;

/**
 * Created by Biagini on 22/03/2017.
 */
public class TagsListAdapter extends ArrayAdapter<String> {
    public TagsListAdapter(Context context, List<String> objects) {
        super(context, R.layout.tag_row, R.id.tag_item, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder;

        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.tag_row, null);
            holder = new ViewHolder();
            holder.tagName = (TextView) v.findViewById(R.id.tag_item);
            holder.originalTagName = (TextView) v.findViewById(R.id.original_tag_item);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        holder.tagName.setText("#" + this.getItem(position).toLowerCase());
        holder.originalTagName.setText(this.getItem(position));

        return v;
    }

}

class ViewHolder {
    TextView tagName;
    TextView originalTagName;
}
