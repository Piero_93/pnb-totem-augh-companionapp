package it.pnbtotem.totemcompanionapp.serversync;

/**
 * NotLoggedException:
 * The user is not logged.
 */
public class NotLoggedException extends Exception {
}
