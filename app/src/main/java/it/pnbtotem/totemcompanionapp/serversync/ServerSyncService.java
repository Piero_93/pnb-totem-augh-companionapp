package it.pnbtotem.totemcompanionapp.serversync;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.util.Pair;
import io.swagger.client.model.RegistrationErrors;
import it.pnbtotem.totemcompanionapp.internetcommunication.ConnectionException;
import it.pnbtotem.totemcompanionapp.internetcommunication.SwaggerClient;
import it.pnbtotem.totemcompanionapp.storage.LocalStorage;
import it.pnbtotem.totemcompanionapp.utilities.StandardService;
import it.pnbtotem.totemcompanionapp.utilities.SysKB;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * ServerSyncService:
 * Service that manage registration and loginUser request, periodic autonomous loginUser, requests for user's tags.
 */
public class ServerSyncService extends StandardService {

    private boolean logged = false;
    private boolean tagsToSave = false;

    /**
     * Execute the login with the stored credentials
     *
     * @return a login result
     */
    private static LoginResult doLogin() {
        Pair<String, String> credentials = LocalStorage.getInstance().getUserAndPw();
        if (credentials.first == null || credentials.second == null) {
            return LoginResult.MISSING_CREDENTIALS;
        }
        int logincode = SwaggerClient.loginUser(credentials.first, credentials.second);
        Log.e("LOGIN CODE", logincode + "");
        switch (logincode) {
            case 200:
                return LoginResult.LOGIN_OK;
            case 503:
            case 0:
                return LoginResult.LOGIN_OFFLINE;
            case 404:
            case 401:
                LocalStorage.getInstance().cleanCredentials();
                return LoginResult.INVALID_CREDENTIALS;
        }

        throw new IllegalArgumentException("Login result is: " + logincode);
    }

    @Override
    protected void prepareHandlers() {

        setOnMessage(new IntentFilter(SysKB.REGISTER_MSG), new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Pair<String, String> creds = LocalStorage.getInstance().getUserAndPw();
                try {
                    RegistrationErrors errors = SwaggerClient.registerNewUser(creds.first, creds.second);
                    if (errors != null) {
                        LocalStorage.getInstance().cleanCredentials();
                    }
                    sendBroadcastMsg(SysKB.REGISTER_RESULT_MSG, errors);
                } catch (ConnectionException e) {
                    e.printStackTrace();
                    if (e.getErrorCode() == 503) {
                        sendBroadcastMsg(SysKB.SERVER_OFFLINE_MSG, null);
                    }
                }
            }
        });


        setOnMessage(new IntentFilter(SysKB.LOGIN_MSG), new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                LoginResult trylogin = doLogin();
                //se non va a buon fine il loginUser invio il risultato e non faccio nient'altro
                logged = (trylogin.equals(LoginResult.LOGIN_OK) || trylogin.equals(LoginResult.LOGIN_OFFLINE));
                sendBroadcastMsg(SysKB.LOGIN_DONE, trylogin);
            }
        });

        setOnMessage(new IntentFilter(SysKB.GET_POSSIBLE_INTERESTS_MSG), new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                    Set<String> tags = SwaggerClient.getAllTags();
                    sendBroadcastMsg(SysKB.RETURN_POSSIBLE_INTERESTS_MSG, tags.toArray());
                } catch (ConnectionException e) {
                    e.printStackTrace();
                    String res = analyseConnectionException(e);
                    switch (res) {
                        case SysKB.LOGIN_DONE:
                            LocalStorage.getInstance().cleanCredentials();
                            sendBroadcastMsg(SysKB.LOGIN_DONE, LoginResult.INVALID_CREDENTIALS);
                            break;
                        default:
                            sendBroadcastMsg(res, null);
                            break;
                    }
                }
            }
        });

        setOnMessage(new IntentFilter(SysKB.SAVE_INTERESTS_MSG), new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Set<String> tags = new HashSet<>();
                Collections.addAll(tags, (String[]) intent.getSerializableExtra("content"));

                LocalStorage.getInstance().setTags(tags);

                try {
                    SwaggerClient.setMyTags(LocalStorage.getInstance().getUserAndPw().first, tags);
                } catch (ConnectionException e) {
                    e.printStackTrace();
                    if (e.getErrorCode() == 404) {
                        sendBroadcastMsg(SysKB.GENERIC_BAD_ERROR_MSG, null);
                    }
                    tagsToSave = true;
                }
            }
        });

        setPeriodic(10000, new Runnable() {
            @Override
            public void run() {
                loginRoutine();
            }
        });

        setOnClose(new Runnable() {
            @Override
            public void run() {
            }
        });
    }

    @Override
    protected String getServiceName() {
        return "ServerSyncService";
    }

    /**
     * Executes periodically the login
     */
    private void loginRoutine() {
        //provo loginUser
        LoginResult trylogin = doLogin();
        //se non va a buon fine il loginUser invio il risultato e non faccio nient'altro
        logged = (trylogin.equals(LoginResult.LOGIN_OK) || trylogin.equals(LoginResult.LOGIN_OFFLINE));
        sendBroadcastMsg(SysKB.LOGIN_DONE, trylogin);
        if (logged) {
            //se loginUser è andato bene controllo che ci siano dei tag da salvare
            if (tagsToSave) {
                //se ci sono tag da salvare
                try {
                    //provo ad aggiornare i tag nel server
                    saveTags(LocalStorage.getInstance().getTags());
                } catch (NotLoggedException e) {
                    //se non è loggato lancio @INVALID_CREDENTIALS
                    sendBroadcastMsg(SysKB.LOGIN_DONE, LoginResult.INVALID_CREDENTIALS);
                    LocalStorage.getInstance().cleanCredentials();
                } catch (ConnectionException e) {
                    //se non va il server lancio @SERVER_OFFLINE_MSG
                    sendBroadcastMsg(SysKB.SERVER_OFFLINE_MSG, null);
                }
            } else {
                //provo a sincronizzare i miei tag col server
                Pair<String, String> credentials = LocalStorage.getInstance().getUserAndPw();
                if(credentials.first != null) {
                    try {
                        //chiedo al server i miei tag e li aggiorno nel ocal storage
                        Set<String> tags = SwaggerClient.fetchMyTags(credentials.first);
                        LocalStorage.getInstance().setTags(tags);
                    } catch (ConnectionException e) {
                        e.printStackTrace();
                        String res = analyseConnectionException(e);
                        switch (res) {
                            case SysKB.LOGIN_DONE:
                                LocalStorage.getInstance().cleanCredentials();
                                sendBroadcastMsg(SysKB.LOGIN_DONE, LoginResult.INVALID_CREDENTIALS);
                                break;
                            default:
                                sendBroadcastMsg(res, null);
                                break;
                        }
                    }
                }
            }
        }
    }

    /**
     * Store tags locally
     *
     * @param tags the set of tags
     * @throws NotLoggedException  when the user is not correctly logged in
     * @throws ConnectionException when network problems occurs
     */
    private void saveTags(Set<String> tags) throws NotLoggedException, ConnectionException {
        if (this.logged) {
            Pair<String, String> credentials = LocalStorage.getInstance().getUserAndPw();
            if (credentials.first == null || credentials.second == null) {
                throw new NotLoggedException();
            }
            try {
                LocalStorage.getInstance().setTags(tags);
                SwaggerClient.setMyTags(credentials.first, tags);
            } catch (ConnectionException e) {
                if (e.getErrorCode() == 404) {
                    throw new ConnectionException(e.getErrorCode(), e.getMessage());
                } else if (e.getErrorCode() == 503 || e.getErrorCode() == 404) {
                    LocalStorage.getInstance().setTags(tags);
                    this.tagsToSave = true;
                } else if (e.getErrorCode() == 404) {
                    throw new NotLoggedException();
                }
            }
        } else {
            throw new NotLoggedException();
        }
    }

    /**
     * Convert the exception to a readable message
     *
     * @param e the exception
     * @return the message
     */
    private String analyseConnectionException(ConnectionException e) {
        if (e.getErrorCode() == 404) {
            //se da 404 killo tutto ritornando @GENERIC_BAD_ERROR_MSG
            return SysKB.GENERIC_BAD_ERROR_MSG;
        } else if (e.getErrorCode() == 503 || e.getErrorCode() == 0) {
            //se non c'è il server ritorno @SERVER_OFFLINE_MSG (non aggiorno il local storage)
            return SysKB.SERVER_OFFLINE_MSG;
        } else if (e.getErrorCode() == 401) {
            //se fa 401 loginUser fallito mando @LOGIN_DONE ad intendere che mancano le credenziali
            return SysKB.LOGIN_DONE;
            //LocalStorage.getInstance().cleanCredentials();
        }
        //throw new IllegalArgumentException("connection error: "+e.getErrorCode());
        return "";
    }

    /**
     * Send a broadcast message
     *
     * @param msg     the message
     * @param content the content
     */
    private void sendBroadcastMsg(String msg, Serializable content) {
        Intent data = new Intent();
        data.setAction(msg);
        if (content != null) {
            data.putExtra("content", content);
        }
        sendMessage(data);
    }

    /**
     * LoginResult:
     * Possible login results
     */
    public enum LoginResult {
        MISSING_CREDENTIALS, LOGIN_OFFLINE, INVALID_CREDENTIALS, LOGIN_OK
    }
}
