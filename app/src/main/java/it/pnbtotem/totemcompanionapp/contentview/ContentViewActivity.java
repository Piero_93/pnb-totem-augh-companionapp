package it.pnbtotem.totemcompanionapp.contentview;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import it.pnbtotem.totemcompanionapp.R;
import it.pnbtotem.totemcompanionapp.contentunpackservice.UnpackService;

import java.io.File;

/**
 * ContentViewActivity:
 * Show a content.
 */
public class ContentViewActivity extends AppCompatActivity {

    public static final String CONTENT_ID_INTENT_KEY = "contentId";
    private int contentId;
    private WebView webView;
    private BroadcastReceiver unpackListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_view);

        webView = (WebView) findViewById(R.id.content_webview);

        WebSettings webSettings = webView.getSettings();
        webView.clearCache(false);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setDomStorageEnabled(false);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                webView.loadUrl(url);
                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.close_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_close:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        Intent intent = getIntent();
        contentId = intent.getIntExtra(CONTENT_ID_INTENT_KEY, -1);
        if (contentId == -1) {
            finish();
            return;
        }

        final int contentIdLocalCopy = contentId;

        unpackListener = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int unpackedId = intent.getIntExtra(UnpackService.UNPACK_CONTENT_ID_KEY, -1);
                if (unpackedId == contentIdLocalCopy) {
                    final boolean result = intent.getBooleanExtra(UnpackService.UNPACK_CONTENT_RESULT, false);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (result) {
                                File index = new File(UnpackService.getUnzipDirectory(getApplicationContext()), "index.html");
                                try {
                                    webView.loadUrl(index.toURI().toURL().toString());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    displayUnpackErrorMessage();
                                    finish();
                                }
                            } else {
                                displayUnpackErrorMessage();
                                finish();
                            }
                        }
                    });

                    unregisterUnpackListener();
                }
            }
        };

        LocalBroadcastManager.getInstance(getApplicationContext())
                .registerReceiver(unpackListener,
                        new IntentFilter(UnpackService.RESPONSE_CONTENT_UNPACKED));

        Intent requestUnpack = new Intent(UnpackService.MSG_DO_UNPACK_CONTENT);
        requestUnpack.putExtra(UnpackService.UNPACK_CONTENT_ID_KEY, contentId);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(requestUnpack);
    }

    @Override
    public void onStop() {
        super.onStop();

        unregisterUnpackListener();
        contentId = -1;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (webView.canGoBack()) {
                        webView.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }

        }

        return super.onKeyDown(keyCode, event);
    }

    private void unregisterUnpackListener() {
        if (unpackListener != null) {
            LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(unpackListener);
            unpackListener = null;
        }
    }

    private void displayUnpackErrorMessage() {
        Toast.makeText(this, R.string.unpack_error, Toast.LENGTH_LONG).show();
    }
}
