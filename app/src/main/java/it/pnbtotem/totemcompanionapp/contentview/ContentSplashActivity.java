package it.pnbtotem.totemcompanionapp.contentview;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Pair;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;
import it.pnbtotem.totemcompanionapp.R;
import it.pnbtotem.totemcompanionapp.contentunpackservice.UnpackService;
import it.pnbtotem.totemcompanionapp.model.ContentMetadata;
import it.pnbtotem.totemcompanionapp.storage.LocalStorage;

import java.util.List;

/**
 * ContentSplashActivity:
 * Splash Screen for contents
 */
public class ContentSplashActivity extends Activity {

    private View background;
    private ImageView splashImage;
    private volatile int contentId = -1;
    private SplashScreenLoaderTask splashScreenLoaderTask;
    private volatile byte[] splashScreenData;
    private BroadcastReceiver unpackListener;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);

        splashImage = (ImageView) findViewById(R.id.splashscreen);

        background = findViewById(R.id.splashBackground);
    }

    @Override
    public void onStart() {
        super.onStart();
        Intent intent = getIntent();
        contentId = intent.getIntExtra(ContentViewActivity.CONTENT_ID_INTENT_KEY, -1);
        if (contentId == -1) {
            finish();
            return;
        }

        ContentMetadata found = null;
        Pair<List<ContentMetadata>, Integer> metadataAndFrameNumber =
                LocalStorage.getInstance().getMetadataAndFrameNum();
        List<ContentMetadata> metadata = metadataAndFrameNumber.first;

        for (ContentMetadata metadatum : metadata) {
            if (contentId == metadatum.getContentId()) {
                found = metadatum;
                break;
            }
        }

        if (found == null) {
            goToViewActivity();
            finish();
            return;
        }

        splashScreenData = found.getSplashScreen();
        if (splashScreenData == null) {
            finish();
            return;
        }

        unpackListener = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int unpackedId = intent.getIntExtra(UnpackService.UNPACK_CONTENT_ID_KEY, -1);
                if (unpackedId == contentId) {
                    final boolean result = intent.getBooleanExtra(UnpackService.UNPACK_CONTENT_RESULT, false);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (result) {
                                goToViewActivity();
                            } else {
                                displayFetchErrorMessage();
                            }

                            finish();
                        }
                    });

                    unregisterUnpackListener();
                }
            }
        };

        LocalBroadcastManager.getInstance(getApplicationContext())
                .registerReceiver(unpackListener,
                        new IntentFilter(UnpackService.RESPONSE_CONTENT_UNPACKED));

        Intent requestUnpack = new Intent(UnpackService.MSG_DO_UNPACK_CONTENT);
        requestUnpack.putExtra(UnpackService.UNPACK_CONTENT_ID_KEY, contentId);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(requestUnpack);

        splashScreenLoaderTask = new SplashScreenLoaderTask();
        splashScreenLoaderTask.execute(contentId);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (splashScreenLoaderTask != null) {
            splashScreenLoaderTask.cancel(false);
            splashScreenLoaderTask = null;
        }

        unregisterUnpackListener();

        splashScreenData = null;
        contentId = -1;
    }

    private void displayFetchErrorMessage() {
        Toast.makeText(this, R.string.splash_screen_error, Toast.LENGTH_LONG).show();
    }

    private void goToViewActivity() {
        Intent openContentActivityIntent = new Intent(
                ContentSplashActivity.this,
                ContentViewActivity.class);

        openContentActivityIntent.putExtra(
                ContentViewActivity.CONTENT_ID_INTENT_KEY,
                contentId);

        startActivity(openContentActivityIntent);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private void unregisterUnpackListener() {
        if (unpackListener != null) {
            LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(unpackListener);
            unpackListener = null;
        }
    }

    private class SplashScreenLoaderTask extends AsyncTask<Integer, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(Integer... integers) {
            try {
                return BitmapFactory.decodeByteArray(splashScreenData, 0, splashScreenData.length);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Bitmap splashScreen) {
            super.onPostExecute(splashScreen);
            if (splashScreen != null) {
                int backgroundColor = splashScreen.getPixel(0, 0);
                background.setBackgroundColor(backgroundColor);

                splashImage.setImageBitmap(splashScreen);
            }
        }
    }
}
