package it.pnbtotem.totemcompanionapp.localcommunication;

import android.content.Intent;
import android.util.Base64;
import android.util.Log;
import android.util.Pair;
import com.google.gson.*;
import it.pnbtotem.totemcompanionapp.model.*;
import it.pnbtotem.totemcompanionapp.storage.LocalStorage;
import it.pnbtotem.totemcompanionapp.utilities.StandardService;
import it.pnbtotem.totemcompanionapp.utilities.SysKB;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.exception.ExceptionUtils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.*;

/**
 * AdvertisingService:
 * Background service that advertise periodically the user to the totem.
 */
public class AdvertisingService extends StandardService {
    private static final int ADVERTISE_DELAY = 7000;
    private static final int ADVERTISE_CONNECTION_TIMEOUT = 5000;
    public static final String ADVERTISING_DONE_MSG = "advDone";
    //private static final boolean DEBUG_USE_FAKE_USER = false;
    //private static final String DEBUG_FAKE_USER = "string";
    //private static final Set<String> DEBUG_FAKE_USER_TAGS = new HashSet<>(Arrays.asList("pizza", "mandolino"));
    private static final Gson customGson = new GsonBuilder().registerTypeHierarchyAdapter(byte[].class,
            new ByteArrayToBase64TypeAdapter()).create();

    private static final String ADVERTISE_ENDPOINT = "advertise";
    private static final String PULL_THUMBNAIL_ENDPOINT = "pullthumbnail";
    private static final String PULL_SPLASH_ENDPOINT = "pullsplash";

    public AdvertisingService() {
    }

    @Override
    protected void prepareHandlers() {
        setPeriodic(ADVERTISE_DELAY, new Runnable() {
            @Override
            public void run() {
                executeJob();
            }
        });
    }

    @Override
    protected String getServiceName() {
        return "Advertising Service";
    }

    private void executeJob() {
        sendDebugString("Starting advertise");
        boolean printStackTrace = false;
        try {
            final Pair<String, String> userAndPw;
            final Set<String> tags;
            //if (DEBUG_USE_FAKE_USER) {
            //    userAndPw = new Pair<>(DEBUG_FAKE_USER, "pass");
            //    tags = new HashSet<>(DEBUG_FAKE_USER_TAGS);
            //} else {
                userAndPw = LocalStorage.getInstance().getUserAndPw();
                Set<String> nullableTagSet = LocalStorage.getInstance().getTags();
                if (nullableTagSet != null) {
                    tags = new HashSet<>(nullableTagSet);
                } else {
                    tags = null;
                }
            //}

            if (userAndPw == null || userAndPw.first == null) {
                throw new IllegalStateException("User not logged in (null values)");
            }

            if (tags == null) {
                throw new IllegalStateException("User has no valid tags (null collection)");
            }

            LocalUserDataBundle userBundle = new LocalUserDataBundle();
            userBundle.setUserId(userAndPw.first);
            userBundle.setTag(tags);

            HttpURLConnection urlConnection = null;
            ResponseToUserAdvertise dataFromTotem;
            try {
                urlConnection = setupConnection(ADVERTISE_ENDPOINT, userBundle);
                printStackTrace = true;
                BufferedInputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String responseAsJson = IOUtils.toString(in, Charset.forName("UTF-8"));
                dataFromTotem = customGson.fromJson(responseAsJson, ResponseToUserAdvertise.class);
                urlConnection.disconnect();
                urlConnection = null;

                List<ContentMetadataWithoutThumbnailAndSplash> contentsOnScreen = dataFromTotem.getContents();
                List<ContentMetadata> previousMetadata = LocalStorage.getInstance().getMetadataAndFrameNum().first;

                List<ContentMetadata> currentContentsMetadata = new ArrayList<>(contentsOnScreen.size());
                List<ContentMetadataWithoutThumbnailAndSplash> missingSplashAndThumbnails = new LinkedList<>();

                for (ContentMetadataWithoutThumbnailAndSplash contentMetadata : contentsOnScreen) {
                    boolean found = false;
                    for (ContentMetadata previousMetadatum : previousMetadata) {
                        if (previousMetadatum.getContentId() == contentMetadata.getContentId()) {
                            currentContentsMetadata.add(previousMetadatum);
                            found = true;
                            break;
                        }
                    }

                    if (!found) {
                        sendDebugString("Data for content " + contentMetadata.getContentId() + " already present");
                        missingSplashAndThumbnails.add(contentMetadata);
                    }
                }

                for (ContentMetadataWithoutThumbnailAndSplash missingSplashAndThumbnail : missingSplashAndThumbnails) {
                    sendDebugString("Fetching missing data for content " + missingSplashAndThumbnail.getContentId());
                    LocalUserIncomingPullRequest request = new LocalUserIncomingPullRequest();
                    request.setUserId(userAndPw.first);
                    request.setContentId(missingSplashAndThumbnail.getContentId());
                    urlConnection = setupConnection(PULL_THUMBNAIL_ENDPOINT, request);

                    in = new BufferedInputStream(urlConnection.getInputStream());
                    byte[] thumbnail = IOUtils.toByteArray(in);
                    urlConnection.disconnect();
                    urlConnection = setupConnection(PULL_SPLASH_ENDPOINT, request);

                    in = new BufferedInputStream(urlConnection.getInputStream());
                    byte[] splash = IOUtils.toByteArray(in);
                    urlConnection.disconnect();
                    urlConnection = null;

                    ContentMetadata metadataWithImages = new ContentMetadata();
                    metadataWithImages.setSplashScreen(splash);
                    metadataWithImages.setThumbnail(thumbnail);
                    metadataWithImages.setContentId(missingSplashAndThumbnail.getContentId());
                    metadataWithImages.setExpirationDate(missingSplashAndThumbnail.getExpirationDate());
                    metadataWithImages.setRev(missingSplashAndThumbnail.getRev());
                    metadataWithImages.setTag(missingSplashAndThumbnail.getTag());
                    metadataWithImages.setUrl(missingSplashAndThumbnail.getUrl());
                    metadataWithImages.setProviderId(missingSplashAndThumbnail.getProviderId());

                    currentContentsMetadata.add(metadataWithImages);
                }

                LocalStorage.getInstance().setContentMetadata(currentContentsMetadata, dataFromTotem.getMaxContentsOnScreen());
                sendMessage(new Intent(ADVERTISING_DONE_MSG));
                sendDebugString("Ended advertise (Ok)");
            } catch (Exception e) {
                LocalStorage.getInstance().setContentMetadata(Collections.<ContentMetadata>emptyList(), 0);
                throw e;
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
        } catch (Exception e) {
            if(printStackTrace) {
                e.printStackTrace();
            } else {
                Log.v("Advertise", "Can't connect to Totem: " + e.getMessage());
            }
            sendDebugString("Ended advertise, failed with exception" + ExceptionUtils.getStackTrace(e));
        }
    }

    private HttpURLConnection setupConnection(String path, Object tobeSent) throws IOException {
        String jsonRep = customGson.toJson(tobeSent);
        sendDebugString("Sending json:\n" + jsonRep);
        byte[] outBytes = jsonRep.getBytes("UTF-8");

        URL url = new URL("http://" + SysKB.TOTEM_HOST + "/" + path);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setConnectTimeout(ADVERTISE_CONNECTION_TIMEOUT);
        urlConnection.setReadTimeout(ADVERTISE_CONNECTION_TIMEOUT);
        urlConnection.setRequestMethod("POST");
        urlConnection.addRequestProperty("Content-Type", "application/json");
        urlConnection.addRequestProperty("Content-Length", "" + outBytes.length);
        urlConnection.setDoInput(true);
        urlConnection.setDoOutput(true);

        try {
            OutputStream outStream = urlConnection.getOutputStream();
            outStream.write(outBytes);

            int responseCode = urlConnection.getResponseCode();
            sendDebugString("Send response code = " + responseCode);
            if (responseCode < 200 || responseCode > 299) {
                throw new IOException("Wrong response code (" + responseCode + ")");
            }

            return urlConnection;
        } catch (Exception e) {
            urlConnection.disconnect();
            throw e;
        }
    }

    /**
     * ByteArrayToBase64TypeAdapter:
     * Convert a ByteArray to and from Base64 using Android's base64 libraries.
     */
    private static class ByteArrayToBase64TypeAdapter implements JsonSerializer<byte[]>, JsonDeserializer<byte[]> {
        public byte[] deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            return Base64.decode(json.getAsString(), Base64.DEFAULT);
        }

        public JsonElement serialize(byte[] src, Type typeOfSrc, JsonSerializationContext context) {
            return new JsonPrimitive(Base64.encodeToString(src, Base64.NO_WRAP));
        }
    }
}
