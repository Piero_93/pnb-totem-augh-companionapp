package it.pnbtotem.totemcompanionapp.localcommunication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.google.gson.Gson;
import it.pnbtotem.totemcompanionapp.model.*;
import it.pnbtotem.totemcompanionapp.storage.LocalStorage;
import it.pnbtotem.totemcompanionapp.utilities.MultipartUtility;
import it.pnbtotem.totemcompanionapp.utilities.StandardService;
import it.pnbtotem.totemcompanionapp.utilities.SysKB;
import org.apache.commons.io.IOUtils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import static it.pnbtotem.totemcompanionapp.utilities.SysKB.TOTEM_HOST;
import static it.pnbtotem.totemcompanionapp.utilities.Utilities.dateToString;
import static it.pnbtotem.totemcompanionapp.utilities.Utilities.stringToDate;

/**
 * SpreaderService:
 * Manages the content spreading between apps and totems.
 */
public class SpreaderService extends StandardService {
    private static final int TIMEOUT = 5000;
    private Gson gson;

    public SpreaderService() {
        gson = new Gson();
    }

    @Override
    protected void prepareHandlers() {
        setOnMessage(new IntentFilter(SysKB.ADVERTISING_DONE_MSG), new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                System.out.println("Spreader started");
                if(LocalStorage.getInstance().getUserAndPw() == null) {
                    return;
                }

                System.out.println("User & pass != null");

                HttpURLConnection urlConnection = null;

                try {
                    List<ContentMetadata> metadata = LocalStorage.getInstance().getAllExistingMetadata();
                    List<ContentMetadataWithoutThumbnailAndSplash> metadataWithoutThumbnailAndSplashes = new ArrayList<>();
                    for (ContentMetadata metadatum : metadata) {
                        ContentMetadataWithoutThumbnailAndSplash newMetadatum = new ContentMetadataWithoutThumbnailAndSplash();
                        newMetadatum.setContentId(metadatum.getContentId());
                        newMetadatum.setProviderId(metadatum.getProviderId());
                        newMetadatum.setTag(metadatum.getTag());
                        newMetadatum.setUrl(metadatum.getUrl());
                        newMetadatum.setExpirationDate(metadatum.getExpirationDate());
                        newMetadatum.setRev(metadatum.getRev());
                        metadataWithoutThumbnailAndSplashes.add(newMetadatum);
                    }
                    LocalUserIncomingSpreadBundle bundle = new LocalUserIncomingSpreadBundle();
                    bundle.setContents(metadataWithoutThumbnailAndSplashes);
                    bundle.setUserId(LocalStorage.getInstance().getUserAndPw().first);

                    urlConnection = setupConnection("spread", bundle);

                    BufferedInputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    String responseAsJson = IOUtils.toString(in, Charset.forName("UTF-8"));

                    ResponseToUserSpread dataFromTotem = new Gson().fromJson(responseAsJson, ResponseToUserSpread.class);

                    urlConnection.disconnect();

                    System.out.println("Contents to send to totem = " + dataFromTotem.getToBeSentToTotem());
                    for (Integer id : dataFromTotem.getToBeSentToTotem()) {
                        Content content = LocalStorage.getInstance().getContent(id);
                        if (content != null) {
                            MultipartUtility multipartUtility = new MultipartUtility("http://" + TOTEM_HOST + "/push", "UTF-8");
                            multipartUtility.addFilePart("contentData", content.getBundle().getContentBytes());

                            ContentMetadataWithoutThumbnailAndSplash metadatum = new ContentMetadataWithoutThumbnailAndSplash();
                            metadatum.setContentId(content.getId());
                            metadatum.setProviderId(content.getProviderId());
                            metadatum.setTag(content.getTags());
                            metadatum.setUrl(content.getUrl());
                            metadatum.setExpirationDate(dateToString(content.getExpirationDate()));
                            metadatum.setRev(content.getRevNumber());

                            multipartUtility.addFilePart("contentMetadata", gson.toJson(metadatum).getBytes("UTF-8"));
                            multipartUtility.finish();
                        }
                    }

                    System.out.println("Contents to receive from totem = " + dataFromTotem.getToBeSentToApp());
                    for (Integer id : dataFromTotem.getToBeSentToApp()) {
                        LocalUserIncomingPullRequest pullRequest = new LocalUserIncomingPullRequest();
                        pullRequest.setContentId(id);
                        pullRequest.setUserId(LocalStorage.getInstance().getUserAndPw().first);
                        urlConnection = setupConnection("pull", pullRequest);

                        in = new BufferedInputStream(urlConnection.getInputStream());
                        ContentBundle contentBundle = new ContentBundleImpl(id, IOUtils.toByteArray(in));
                        in.close();

                        urlConnection.disconnect();

                        urlConnection = setupConnection("pullmetadata", pullRequest);

                        in = new BufferedInputStream(urlConnection.getInputStream());
                        responseAsJson = IOUtils.toString(in, Charset.forName("UTF-8"));
                        ContentMetadataWithoutThumbnailAndSplash metadataFromTotem = new Gson().fromJson(responseAsJson, ContentMetadataWithoutThumbnailAndSplash.class);


                        LocalStorage.getInstance().saveContent(new Content(
                                metadataFromTotem.getContentId(),
                                metadataFromTotem.getProviderId(),
                                metadataFromTotem.getTag(),
                                metadataFromTotem.getUrl(),
                                stringToDate(metadataFromTotem.getExpirationDate()),
                                metadataFromTotem.getRev(),
                                contentBundle
                        ));

                        urlConnection.disconnect();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                }
            }
        });
    }

    @Override
    protected String getServiceName() {
        return "SpreaderService";
    }

    private HttpURLConnection setupConnection(String path, Object tobeSent) throws IOException {
        String jsonRep = gson.toJson(tobeSent);
        sendDebugString("Sending json:\n" + jsonRep);
        System.out.println("Sending json:\n" + jsonRep);
        byte[] outBytes = jsonRep.getBytes("UTF-8");

        URL url = new URL("http://" + TOTEM_HOST + "/" + path);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setConnectTimeout(TIMEOUT);
        urlConnection.setReadTimeout(TIMEOUT);
        urlConnection.setRequestMethod("POST");
        urlConnection.addRequestProperty("Content-Type", "application/json; charset=utf-8");
        urlConnection.addRequestProperty("Content-Length", "" + outBytes.length);
        urlConnection.setDoInput(true);
        urlConnection.setDoOutput(true);

        try {
            OutputStream outStream = urlConnection.getOutputStream();
            outStream.write(outBytes);

            int responseCode = urlConnection.getResponseCode();
            sendDebugString("Send response code = " + responseCode);
            if (responseCode < 200 || responseCode > 299) {
                throw new IOException("Wrong response code (" + responseCode + ")");
            }

            return urlConnection;
        } catch (Exception e) {
            urlConnection.disconnect();
            throw e;
        }
    }

    /*private byte[] contentToByteArray(Content content) throws IOException {
        ZipFile zipFile = content.getBundle().getContentZip();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ZipOutputStream zipOutputStream = new ZipOutputStream(bos);
        Enumeration<? extends ZipEntry> enumeration = zipFile.entries();
        while (enumeration.hasMoreElements()) {
            ZipEntry e = enumeration.nextElement();
            e.setCompressedSize(-1);
            zipOutputStream.putNextEntry(e);
            zipOutputStream.closeEntry();
        }
        zipOutputStream.close();

        return bos.toByteArray();
    }*/
}
